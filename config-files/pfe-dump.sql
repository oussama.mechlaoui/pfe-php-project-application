-- MariaDB dump 10.17  Distrib 10.4.7-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pfe
-- ------------------------------------------------------
-- Server version	10.4.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `act` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `matricule` varchar(255) NOT NULL,
  `matfis` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  KEY `matfis` (`matfis`),
  KEY `actions_ibfk_2` (`matricule`),
  CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`matfis`) REFERENCES `clients` (`matfis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES ('Suspect','2016-08-17','interesse;','122','22','Ben Arous'),('Commande','2016-08-10','non interesse','263','2500','Ben Arous'),('Suspect','2016-08-17','interesse','122','89','Ben Arous'),('Negociation','2016-08-23','interesse','122','22','Ben Arous'),('Approche','2016-08-23','pas de reponse','122','89','Ben Arous');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id_cat` int(100) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Fixe','Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre Fixe et d\'activer les services supplÃ©mentaires.'),(2,'Mobile a Facture','	Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre Mobile a Facture et d\'activer les services supplÃ©mentaires.'),(3,'3G','Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre 3G et d\'activer les services supplÃ©mentaires.'),(4,'GSM','	Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre GSM et d\'activer les services supplÃ©mentaires.'),(5,'Production Fixe','Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre Production Fixe et d\'activer les services supplÃ©mentaires.'),(6,'Production Mobile','Cette categorie vous permet d\'accÃ©der Ã  la liste des offres permettant de gÃ©rer votre Production Mobile et d\'activer les services supplÃ©mentaires.');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientele`
--

DROP TABLE IF EXISTS `clientele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientele` (
  `id_clientele` int(100) NOT NULL AUTO_INCREMENT,
  `matricule` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  PRIMARY KEY (`id_clientele`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientele`
--

LOCK TABLES `clientele` WRITE;
/*!40000 ALTER TABLE `clientele` DISABLE KEYS */;
INSERT INTO `clientele` VALUES (1,'122','Oussama','mechaloui','Ben Arous'),(2,'263','Brichni','Raouf','Ben Arous'),(3,'007','Nadaa','taieb','Ben Arous');
/*!40000 ALTER TABLE `clientele` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `rs` varchar(255) NOT NULL,
  `rc` varchar(255) NOT NULL,
  `matfis` varchar(255) NOT NULL,
  `mat` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `gouv` varchar(255) NOT NULL,
  `cp` varchar(255) NOT NULL,
  `activite` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  PRIMARY KEY (`matfis`),
  KEY `fk_mat` (`mat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES ('Aramex','transit                     ','22','122','15 street','ben arous','2050','import export','Ben Arous'),('TT','opÃ©rateur   ','2500','263','15 street','ben arous','2050','telephonie','Ben Arous'),('POST','Fabric      ','89','122','15 street','ben arous','2050                              ','Textile','Ben Arous');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrats`
--

DROP TABLE IF EXISTS `contrats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrats` (
  `id_contrat` int(100) NOT NULL AUTO_INCREMENT,
  `code` int(100) NOT NULL,
  `matfis` varchar(255) NOT NULL,
  `id_cc` int(100) NOT NULL,
  `type` varchar(255) NOT NULL,
  `etat` varchar(255) NOT NULL,
  `signature` date NOT NULL,
  `duree` varchar(255) NOT NULL,
  `fin` date NOT NULL,
  `nbre_offres` int(100) NOT NULL,
  `ca_mensuel` int(100) NOT NULL,
  `remise` varchar(255) NOT NULL,
  `geste` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  PRIMARY KEY (`id_contrat`),
  KEY `matfis` (`matfis`),
  KEY `fk_idcc` (`id_cc`),
  CONSTRAINT `contrats_ibfk_1` FOREIGN KEY (`matfis`) REFERENCES `clients` (`matfis`),
  CONSTRAINT `fk_idcc` FOREIGN KEY (`id_cc`) REFERENCES `clientele` (`id_clientele`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrats`
--

LOCK TABLES `contrats` WRITE;
/*!40000 ALTER TABLE `contrats` DISABLE KEYS */;
INSERT INTO `contrats` VALUES (1,28,'2500',2,'mobile','on','2016-08-01','1','2016-08-31',2,100000,'10%','tablette','Ben Arous'),(2,55,'22',1,'mobile','on','2016-08-01','1','2016-08-31',5,200000,'26%','iphone','Ben Arous'),(3,55,'22',1,'fixe','on','2016-08-01','1 mois','2016-08-31',3,200000,'80%','tablette','Ben Arous'),(4,28,'2500',2,'fixe','on','2016-08-01','1 mois','2016-08-31',2,200000,'49%','imprimante','Ben Arous'),(5,254,'22',1,'data','on','2016-08-01','2 mois','2016-09-30',2,20525000,'20%','samsung','Ben Arous'),(6,28,'2500',2,'data','on','2016-08-01','2 mois','2016-09-30',1,200000,'7%','tablette','Ben Arous');
/*!40000 ALTER TABLE `contrats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacett`
--

DROP TABLE IF EXISTS `espacett`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacett` (
  `id_espace` int(11) NOT NULL AUTO_INCREMENT,
  `espace` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id_espace`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacett`
--

LOCK TABLES `espacett` WRITE;
/*!40000 ALTER TABLE `espacett` DISABLE KEYS */;
INSERT INTO `espacett` VALUES (1,'Ezzahra','Ben Arous',' Espace TT : Ezzahra'),(2,'Ben Arous','Ben Arous',' Espace TT : Ben Arous'),(3,'Mourouj','Ben Arous',' Espace TT : Mourouj '),(4,'Rades','Ben Arous','Espace TT : Rades'),(5,'La marsa','Tunis','Espace TT : La Marsa'),(6,'sfax jadida','Sfax','sfax jadida');
/*!40000 ALTER TABLE `espacett` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forfait`
--

DROP TABLE IF EXISTS `forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forfait` (
  `id_forfait` int(100) NOT NULL AUTO_INCREMENT,
  `forfait` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `offre` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `etat` varchar(255) NOT NULL,
  PRIMARY KEY (`id_forfait`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forfait`
--

LOCK TABLES `forfait` WRITE;
/*!40000 ALTER TABLE `forfait` DISABLE KEYS */;
INSERT INTO `forfait` VALUES (1,'ligne de 120DT','privilège','mobile','offre mobile','active'),(2,'ligne de 100dt','MObileTT','mobile','offre mobile','active'),(3,'ligne de 100dt','privilï¿½ge','mobile','offre mobile','active'),(4,'Net Box de TT','Tres Haut debit','data','Offre trÃ¨s haute dÃ©bit','active');
/*!40000 ALTER TABLE `forfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interlocuteur`
--

DROP TABLE IF EXISTS `interlocuteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interlocuteur` (
  `id_int` int(100) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `piece` varchar(255) NOT NULL,
  `identite` varchar(255) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `fixe` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `matfis` varchar(255) NOT NULL,
  PRIMARY KEY (`id_int`),
  KEY `fk_matfis` (`matfis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interlocuteur`
--

LOCK TABLES `interlocuteur` WRITE;
/*!40000 ALTER TABLE `interlocuteur` DISABLE KEYS */;
INSERT INTO `interlocuteur` VALUES (1,'Business','Business','cin','09617974   ','Manager','71456789','98963852','meccch@yahoo.fr','22'),(2,'Bri','Ra','cin','569742','Subdivision SI','2147483647','454545','birchi@gmail.com','2500'),(3,'Bra','azer','cin','2132026      ','Financier','5643216515','4545455','isisisi@gmail.com','89');
/*!40000 ALTER TABLE `interlocuteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objectifs`
--

DROP TABLE IF EXISTS `objectifs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objectifs` (
  `id_objectif` int(100) NOT NULL AUTO_INCREMENT,
  `objectifsespace1` int(100) NOT NULL,
  `objectifsespace2` int(100) DEFAULT NULL,
  `objectifsregion1` int(100) NOT NULL,
  `objectifsregion2` int(100) DEFAULT NULL,
  `categorie` varchar(255) NOT NULL,
  `offre` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  PRIMARY KEY (`id_objectif`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objectifs`
--

LOCK TABLES `objectifs` WRITE;
/*!40000 ALTER TABLE `objectifs` DISABLE KEYS */;
INSERT INTO `objectifs` VALUES (1,500,625,2000,2500,'Fixe','coupon 10DT','2016','Ben Arous'),(2,625,625,2500,2500,'Fixe','coupon 30DT','2016','Ben Arous'),(3,500,625,2000,2500,'Fixe','coupon 20DT','2016','Ben Arous'),(4,625,625,2500,2500,'Mobile a Facture','Forfait Mobi TT','2016','Ben Arous'),(5,700,700,2800,2800,'Mobile a Facture','optimum +','2016','Ben Arous'),(6,400,400,1600,1600,'3G','Fixe Ni','2016','Ben Arous'),(7,500,500,2500,2500,'Fixe','coupon 10DT','2019','Ben Arous');
/*!40000 ALTER TABLE `objectifs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offres`
--

DROP TABLE IF EXISTS `offres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offres` (
  `id_offre` int(11) NOT NULL AUTO_INCREMENT,
  `id_cat` int(100) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `etat` varchar(255) NOT NULL,
  PRIMARY KEY (`id_offre`),
  KEY `id_cat` (`id_cat`),
  CONSTRAINT `offres_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `categories` (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offres`
--

LOCK TABLES `offres` WRITE;
/*!40000 ALTER TABLE `offres` DISABLE KEYS */;
INSERT INTO `offres` VALUES (1,1,'coupon 10DT','coupon Fixe 10DT','active'),(2,1,'coupon 30DT','coupon Fixe 30DT','active'),(3,2,'Forfait Mobi TT','Forfait Mobile a facture Mobi TT','active'),(4,3,'Fixe Ni','Forfait Ni 3G','active'),(5,2,'test','test','inactive'),(6,3,'ClÃ© link top','ClÃ© 3G','active'),(7,5,'Transfert','Transfert Fixe','active'),(8,6,'CSS Mobile','Css mobile','active'),(9,2,'optimum +','vgfui','active'),(10,1,'coupon 20DT','coupon 20 DT Fixe','active'),(11,4,'GSMcash','Offre gsmCash','active');
/*!40000 ALTER TABLE `offres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `souscontrats`
--

DROP TABLE IF EXISTS `souscontrats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `souscontrats` (
  `id_sc` int(100) NOT NULL AUTO_INCREMENT,
  `id_contrat` int(100) NOT NULL,
  `code_client` int(100) NOT NULL,
  `matfis` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `nbre_lignes` int(100) NOT NULL,
  `offre` varchar(255) NOT NULL,
  PRIMARY KEY (`id_sc`),
  KEY `id_contrat` (`id_contrat`),
  KEY `matfis` (`matfis`),
  CONSTRAINT `souscontrats_ibfk_1` FOREIGN KEY (`id_contrat`) REFERENCES `contrats` (`id_contrat`),
  CONSTRAINT `souscontrats_ibfk_2` FOREIGN KEY (`matfis`) REFERENCES `contrats` (`matfis`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `souscontrats`
--

LOCK TABLES `souscontrats` WRITE;
/*!40000 ALTER TABLE `souscontrats` DISABLE KEYS */;
INSERT INTO `souscontrats` VALUES (1,1,28,'2500','mobile',4,'Transfert'),(2,1,28,'2500','mobile',20,'CSS Mobile'),(3,3,55,'22','fixe',25,'coupon 20DT'),(4,3,55,'22','fixe',30,'coupon 10DT'),(5,2,55,'22','mobile',10,'ClÃ© link top'),(6,2,55,'22','mobile',8,'test'),(7,2,55,'22','mobile',1,'Forfait Mobi TT'),(8,3,55,'22','mobile',8,'coupon 10DT'),(9,3,55,'22','mobile',2,'coupon 30DT'),(10,3,55,'22','mobile',15,'Transfert'),(11,4,28,'2500','fixe',20,'optimum +'),(12,4,28,'2500','fixe',10,'Forfait Mobi TT'),(13,5,254,'22','data',100,'optimum +'),(14,5,254,'22','data',50,'ClÃ© link top'),(15,6,28,'2500','data',10,'Net Box de TT');
/*!40000 ALTER TABLE `souscontrats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profil` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `espace` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (22,'ttagence','azerty','agence','Ag','EZ','Ezzahra','Ben Arous'),(33,'ttchef','azerty','chef','Bri','Bri','','Ben Arous'),(52,'ttadmin','azerty','admin','Mechlaoui','Oussama','',''),(53,'radestt','azerty','agence','espacett','rades','Rades','Ben Arous'),(54,'chefchef','azerty','chef','chef','mech','La marsa','Tunis'),(55,'agag','azerty','agence','Dali','TT','La marsa','Tunis'),(56,'testadmin','azerty','admin','Admin','Admin','','Ben Arous');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventes`
--

DROP TABLE IF EXISTS `ventes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventes` (
  `id_vente` int(100) NOT NULL AUTO_INCREMENT,
  `offre` varchar(255) NOT NULL,
  `quantite` int(200) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `espacett` varchar(255) NOT NULL,
  `DRT` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id_vente`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventes`
--

LOCK TABLES `ventes` WRITE;
/*!40000 ALTER TABLE `ventes` DISABLE KEYS */;
INSERT INTO `ventes` VALUES (2,'coupon 30DT',25,'Fixe','Ezzahra','Ben Arous','2016-07-10'),(3,'Forfait Mobi TT',15,'Mobile a Facture','Ezzahra','Ben Arous','2016-07-10'),(4,'test',19,'Mobile a Facture','Ezzahra','Ben Arous','2016-07-10'),(5,'Fixe Ni',299,'3G','Ezzahra','Ben Arous','2016-07-10'),(6,'ClÃ© link top',255,'3G','Ezzahra','Ben Arous','2016-07-10'),(7,'Transfert',14,'Production Fixe','Ezzahra','Ben Arous','2016-06-05'),(8,'CSS Mobile',15,'Production Mobile','Ezzahra','Ben Arous','2016-07-10'),(10,'coupon 30DT',39,'Fixe','Ezzahra','Ben Arous','2016-07-11'),(12,'coupon 30DT',200,'Fixe','Rades','Ben Arous','2016-08-01'),(13,'Fixe Ni',88,'3G','Rades','Ben Arous','2016-08-01'),(14,'Transfert',25,'Production Fixe','Rades','Ben Arous','2016-08-01'),(18,'coupon 30DT',18,'Fixe','Ezzahra','Ben Arous','2016-08-17'),(19,'coupon 20DT',20,'Fixe','Ezzahra','Ben Arous','2016-08-17'),(20,'Forfait Mobi TT',3,'Mobile a Facture','Ezzahra','Ben Arous','2016-08-17'),(21,'optimum +',5,'Mobile a Facture','Ezzahra','Ben Arous','2016-08-17'),(22,'Fixe Ni',6,'3G','Ezzahra','Ben Arous','2016-08-17'),(23,'ClÃ© link top',8,'3G','Ezzahra','Ben Arous','2016-08-17'),(24,'GSMcash',12,'GSM','Ezzahra','Ben Arous','2016-08-17'),(25,'Transfert',40,'Production Fixe','Ezzahra','Ben Arous','2016-08-17'),(26,'CSS Mobile',33,'Production Mobile','Ezzahra','Ben Arous','2016-08-17'),(28,'optimum +',45,'Mobile a Facture','Ezzahra','Ben Arous','2016-08-21'),(29,'CSS Mobile',20,'Production Mobile','Ezzahra','Ben Arous','2016-08-21'),(32,'coupon 30DT',140,'Fixe','Ezzahra','Ben Arous','2016-09-13'),(33,'coupon 20DT',120,'Fixe','Ezzahra','Ben Arous','2016-09-13'),(34,'Forfait Mobi TT',80,'Mobile a Facture','Ezzahra','Ben Arous','2016-09-13'),(35,'optimum +',50,'Mobile a Facture','Ezzahra','Ben Arous','2016-09-13'),(36,'Fixe Ni',65,'3G','Ezzahra','Ben Arous','2016-09-13'),(37,'ClÃ© link top',60,'3G','Ezzahra','Ben Arous','2016-09-13'),(38,'GSMcash',30,'GSM','Ezzahra','Ben Arous','2016-09-13'),(39,'Transfert',20,'Production Fixe','Ezzahra','Ben Arous','2016-09-13'),(40,'CSS Mobile',23,'Production Mobile','Ezzahra','Ben Arous','2016-09-13'),(42,'coupon 30DT',10,'Fixe','Ezzahra','Ben Arous','2019-07-15'),(43,'coupon 20DT',25,'Fixe','Ezzahra','Ben Arous','2019-07-15'),(44,'Forfait Mobi TT',5,'Mobile a Facture','Ezzahra','Ben Arous','2019-07-15'),(45,'optimum +',8,'Mobile a Facture','Ezzahra','Ben Arous','2019-07-15'),(46,'Fixe Ni',6,'3G','Ezzahra','Ben Arous','2019-07-15'),(47,'ClÃ© link top',12,'3G','Ezzahra','Ben Arous','2019-07-15'),(48,'GSMcash',38,'GSM','Ezzahra','Ben Arous','2019-07-15'),(49,'Transfert',100,'Production Fixe','Ezzahra','Ben Arous','2019-07-15'),(50,'CSS Mobile',69,'Production Mobile','Ezzahra','Ben Arous','2019-07-15'),(51,'coupon 10DT',50,'Fixe','Ezzahra','Ben Arous','2019-07-15');
/*!40000 ALTER TABLE `ventes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30 20:48:43
