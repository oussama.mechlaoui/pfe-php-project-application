MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="//"

--//
Content-Type: text/x-shellscript; charset="us-ascii"
#!/bin/bash

#Install SELinux for worker and docker
amazon-linux-extras install selinux-ng -y
yum install container-selinux -y
#Enforce SELinux for worker and docker
sed -i s,SELINUX=disabled,SELINUX=enforcing,g /etc/selinux/config
sed '8 a "selinux-enabled": true,' /etc/docker/daemon.json

#Download SELinux custom policies

aws s3 cp s3://eks-pfe-cluster-files/policies.tar.gz .
tar xvfz policies.tar.gz

#Add SELinux modules
semodule -i policies/*

set -ex
B64_CLUSTER_CA=LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRJd01Ea3pNREl5TURjek9Gb1hEVE13TURreU9ESXlNRGN6T0Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTGM2CkJ1TWZFb25pVEhwZnRpSmNHRVJVUWd6RUZDQ0VORzhqdE1qdU5TR3ZPUnBNQmd3TTRSQ0dKek92ZUJhYjNTckIKOEY0VVlBbVRoNEtadFM0djJKQ3BQL3h1K2Y1RDZuNzlCVjdrSFJEMjZkY2NsWFZSV3l6RTZncFpYeGNuQVhYNgpFY3UvblRKalgyYis2WlVNbE1LOHgrbjZBWFpyTkF4RDZoRUlTV3dsOHBydFhoNTNXWEtybDJ4Y0x3NWhFVFRrCmU1SlBnR2VFK0pYNnVTUzJya1MvT0V3SVA5WjNhcytHRTExTmRmckpaNDh6a1d5VnZXZDJkNGFpWHZoMlhxTG8KMlZGZUlnVC9JT0UyelNRK05wUytQNE1hWlNzVlNYSkk2a21qc05CUTBjZC9hNmd0TktiaC9IbkRLVjRQWjhqYgp4L3Z0MzA2Y2YrS0NXbEJSOXFNQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFJUkZRemUrdEtmTjN3Zzh1QTNwclpoRGZNczQKUTRnKzlaV3lLbkRHVjlneWw1S0dwQUZxMTBlSmlHOFhDbUNtSUFEYkVKMEUzMFF0YjYwZGFnYUU0eXREN3lxRQpOVkoyNUs5M1BPeWRpQUcrM2RLWkdjSGQ2b0x6ZnNmQ0Q1d01jODN0NE9MTUZsM1FoZ2NBZURnSThyZ0pwZUU1CnFOS1dOb2dOL21iWGZubUh0Um9RNm9hZmVuOVJzU3pqRWk3K2k0VEc1dkI5bCsxUGVjNnFYSWF1ejN0UDlEK3gKNFc0RURlTW0rYTkxMnZHc3BvbzV1UWU1YnlGWXBFQ0tNQXJHOU1sMnRGd3A5WkVLbXBMRUEvSlluRGgzWk5wSApuU0R4WTdEWTVhR1RKU1dGNkNJb2xjTHMxTjBLU3VIYjdFald1ZTJLZ041T3RmK05mM0poQUZhS2oyMD0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
API_SERVER_URL=https://CD4FEDFCF55AB6322C78348BE7A673D4.gr7.eu-west-1.eks.amazonaws.com
K8S_CLUSTER_DNS_IP=10.100.0.10
/etc/eks/bootstrap.sh pfe-cluster --kubelet-extra-args '--node-labels=alpha.eksctl.io/cluster-name=pfe-cluster,alpha.eksctl.io/nodegroup-name=managed-node-group,eks.amazonaws.com/nodegroup=managed-node-group,eks.amazonaws.com/nodegroup-image=ami-08984e9a930a5092e,eks.amazonaws.com/sourceLaunchTemplateId=lt-094f8fa02a7709390,eks.amazonaws.com/sourceLaunchTemplateVersion=1' --b64-cluster-ca $B64_CLUSTER_CA --apiserver-endpoint $API_SERVER_URL --dns-cluster-ip $K8S_CLUSTER_DNS_IP

reboot

--//--