  local grafana = import 'grafonnet/grafana.libsonnet';
  local dashboard = grafana.dashboard;
  local row = grafana.row;
  local prometheus = grafana.prometheus;
  local template = grafana.template;
  local graphPanel = grafana.graphPanel;
  
  local k = import 'ksonnet/ksonnet.beta.3/k.libsonnet';

  local kp = (import 'kube-prometheus/main.libsonnet') +
             (import 'kube-prometheus/addons/node-ports.libsonnet') +
  {
    values+:: {
      common+: {
        namespace: 'monitoring',
        platform: 'eks'
      },
      prometheus+:: {
        namespaces+: ['default','pfe-project','kube-system'],
      },
      grafana+:: {
        dashboards+:: {  // use this method to import your dashboards to Grafana
          'my-dashboard.json': (import 'rendered/coredns.json'),
         },
        config: { // http://docs.grafana.org/installation/configuration/
          sections: {
            "security" : {
               'admin_user': 'project-admin',
               'admin_password': 'Monitoring@12',
            }
            
          },
        },
      },
      
    },
    
    grafana+:: {
      dashboards+:: {
        'my-dashboard.json':
          dashboard.new('My Dashboard')
          .addTemplate(
            {
              current: {
                text: 'Prometheus',
                value: 'Prometheus',
              },
              hide: 0,
              label: null,
              name: 'datasource',
              options: [],
              query: 'prometheus',
              refresh: 1,
              regex: '',
              type: 'datasource',
            },
          )
          .addRow(
            row.new()
            .addPanel(graphPanel.new('My Panel', span=6, datasource='$datasource')
                      .addTarget(prometheus.target('vector(1)')))
          ),
      },
    },
    
    ingress+:: {
      'prometheus-k8s': {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: $.prometheus.prometheus.metadata.name,
          namespace: $.prometheus.prometheus.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': 'arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',            
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'prometheus.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: 'ssl-redirect',
                    port: {
                      name: 'use-annotation'
                    },
                  },
                },
              },
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: $.prometheus.service.metadata.name,
                    port: {
                      number: 9090
                    },
                  },
                },
              },
              ],
            },
          },
          ],
        },
      },
      
      'altermanager': {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: $.alertmanager.alertmanager.metadata.name,
          namespace: $.alertmanager.alertmanager.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': 'arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'alertmanager.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: 'ssl-redirect',
                    port: {
                      name: 'use-annotation'
                    },
                  },
                },
              },
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: $.alertmanager.service.metadata.name,
                    port: {
                      number: 9093
                    },
                  },
                },
              },
              ],
            },
          },
          ],
        },
      },
      
      'grafana': {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: 'grafana-ingress',
          namespace: $.prometheus.prometheus.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': 'arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'grafana.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: 'ssl-redirect',
                    port: {
                      name: 'use-annotation'
                    },
                  },
                },
              },
              {
                path: '/*',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: $.grafana.service.metadata.name,
                    port: {
                      number: 3000
                    },
                  },
                },
              },
              ],
            },
          },
          ],
        },
      }, 
    },
    
    // Configure External URL's per application
    alertmanager+:: {
      alertmanager+: {
        spec+: {
          externalUrl: 'https://alertmanager.monitoring.eks.pfe-project-graduation.cloud/',
        },
      },
    },
    
    prometheus+:: {
      serviceAccount+: {
        metadata+: {
          annotations+: {
            'eks.amazonaws.com/role-arn': 'arn:aws:iam::538931824939:role/cloudwatch-irsa',
            },
      },
      },
      prometheus+: {
        spec+: {
          securityContext: {
            seLinuxOptions: {
              type: 'spc_t'
            },
          },
          externalUrl: 'https://prometheus.monitoring.eks.pfe-project-graduation.cloud/',
          storage: {
            volumeClaimTemplate: {
              spec: {
                accessModes: ['ReadWriteMany'],
                volumeMode: 'Filesystem',
                resources: {
                  requests: {
                    storage: '20Gi'
                  },
                },
                storageClassName: 'efs-sc-dynamic'
              },
            },
          },
        },
      },
      serviceMonitorCloudWatch: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'cw-exporter-servicemonitor',
        namespace: 'monitoring',
      },
      spec: {
        jobLabel: 'app',
        endpoints: [
          {
            port: 'http',
            path: 'metrics',
            interval: '30s',
          },
        ],
        selector: {
          matchLabels: {
            app: 'prometheus-cloudwatch-exporter',
          },
        },
      },
      },
      serviceMonitorCoreDNS: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'coredns-servicemonitor',
        namespace: 'kube-system',
      },
      spec: {
        jobLabel: 'app',
        endpoints: [
          {
            port: 'prometheus',
            path: 'metrics',
            interval: '30s',
          },
        ],
        selector: {
          matchLabels: {
            'k8s-app': 'kube-dns',
          },
        },
      },
      },
    },
  };
    
{ 'setup/0namespace-namespace': kp.kubePrometheus.namespace } +
{
  ['setup/prometheus-operator-' + name]: kp.prometheusOperator[name]
  for name in std.filter((function(name) name != 'serviceMonitor' && name != 'prometheusRule'), std.objectFields(kp.prometheusOperator))
} +
// serviceMonitor and prometheusRule are separated so that they can be created after the CRDs are ready
{ 'prometheus-operator-serviceMonitor': kp.prometheusOperator.serviceMonitor } +
{ 'prometheus-operator-prometheusRule': kp.prometheusOperator.prometheusRule } +
{ 'kube-prometheus-prometheusRule': kp.kubePrometheus.prometheusRule } +

{ ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
{ ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
{ ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
{ ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
{ ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
{ ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) } +
{ ['ingress-' + name]: kp.ingress[name] for name in std.objectFields(kp.ingress) }
