  local grafana = import 'grafonnet/grafana.libsonnet';
  local dashboard = grafana.dashboard;
  local row = grafana.row;
  local prometheus = grafana.prometheus;
  local template = grafana.template;
  local graphPanel = grafana.graphPanel;
  
  local k = import 'ksonnet/ksonnet.beta.3/k.libsonnet';
  local pvc = k.core.v1.persistentVolumeClaim;

  local kp = (import 'kube-prometheus/kube-prometheus.libsonnet') + 
             (import 'kube-prometheus/kube-prometheus-eks.libsonnet') + 
             (import 'kube-prometheus/kube-prometheus-node-ports.libsonnet') + 
             (import 'kube-prometheus/kube-prometheus-anti-affinity.libsonnet') 
  {
    _config+:: {
      namespace: 'monitoring',
      prometheus+:: {
        namespaces+: ['pfe-project','amazon-cloudwatch'],
      },
      grafana+:: {
        config: { // http://docs.grafana.org/installation/configuration/
          sections: {
            "server" : {root_url: 'https://grafana.monitoring.eks.pfe-project-graduation.cloud/'},
            "security" : {
               'admin_user': 'project-admin',
               'admin_password': 'Monitoring@12',
            }
            
          },
        },
      },
      
    },
    
    ingress+:: {
      'prometheus-k8s': {
        apiVersion: 'extensions/v1beta1',
        kind: 'Ingress',
        metadata: {
          name: $.prometheus.prometheus.metadata.name,
          namespace: $.prometheus.prometheus.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': ' arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',            
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'prometheus.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                backend: {
                  serviceName: 'ssl-redirect',
                  servicePort: 'use-annotation',
                },
              },
              {
                path: '/*',
                backend: {
                  serviceName: $.prometheus.service.metadata.name,
                  servicePort: 9090,
                },
              },
              ],
            },
          },
          ],
        },
      },
      
      'altermanager': {
        apiVersion: 'extensions/v1beta1',
        kind: 'Ingress',
        metadata: {
          name: $.alertmanager.alertmanager.metadata.name,
          namespace: $.alertmanager.alertmanager.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': ' arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'alertmanager.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                backend: {
                  serviceName: 'ssl-redirect',
                  servicePort: 'use-annotation',
                },
              },
              {
                path: '/*',
                backend: {
                  serviceName: $.alertmanager.service.metadata.name,
                  servicePort: 9093,
                },
              },
              ],
            },
          },
          ],
        },
      },
      
      'grafana': {
        apiVersion: 'extensions/v1beta1',
        kind: 'Ingeress',
        metadata: {
          name: 'grafana-ingress',
          namespace: $.prometheus.prometheus.metadata.namespace,
          annotations: {
            'kubernetes.io/ingress.class': 'alb',
            'alb.ingress.kubernetes.io/scheme': 'internet-facing',
            'alb.ingress.kubernetes.io/certificate-arn': ' arn:aws:acm:eu-west-1:538931824939:certificate/d8f222d5-4be6-4b14-a1c1-522854a6ab30',
            'alb.ingress.kubernetes.io/listen-ports': '[{"HTTP": 80}, {"HTTPS":443}]',
            'alb.ingress.kubernetes.io/actions.ssl-redirect': '{"Type": "redirect","RedirectConfig":{"Protocol":"HTTPS","Port":"443","StatusCode":"HTTP_301"}}',
            'alb.ingress.kubernetes.io/security-groups': 'sg-0c7469cb5feaec7ec',
            'alb.ingress.kubernetes.io/group.name': 'eks-monitoring',
            'alb.ingress.kubernetes.io/success-codes': '200-302',
          },
        },
        spec: {
          rules: [{
            host: 'grafana.monitoring.eks.pfe-project-graduation.cloud',
            http: {
              paths: [
              {
                path: '/*',
                backend: {
                  serviceName: 'ssl-redirect',
                  servicePort: 'use-annotation',
                },
              },
              {
                path: '/*',
                backend: {
                  serviceName: $.grafana.service.metadata.name,
                  servicePort: 3000,
                },
              },
              ],
            },
          },
          ],
        },
      },
      
      
    },
    
    grafana+:: {
      dashboards+:: {
        'my-dashboard.json':
          dashboard.new('My Dashboard')
          .addTemplate(
            {
              current: {
                text: 'Prometheus',
                value: 'Prometheus',
              },
              hide: 0,
              label: null,
              name: 'datasource',
              options: [],
              query: 'prometheus',
              refresh: 1,
              regex: '',
              type: 'datasource',
            },
          )
          .addRow(
            row.new()
            .addPanel(graphPanel.new('My Panel', span=6, datasource='$datasource')
                      .addTarget(prometheus.target('vector(1)')))
          ),
      },
    },
    
    // Configure External URL's per application
    alertmanager+:: {
      alertmanager+: {
        spec+: {
          externalUrl: 'https://alertmanager.monitoring.eks.pfe-project-graduation.cloud/',
        },
      },
    },
    
    prometheus+:: {
      prometheus+: {
        spec+: {
          externalUrl: 'https://prometheus.monitoring.eks.pfe-project-graduation.cloud/',
        },
      },
      serviceMonitorCloudWatch: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'cw-exporter-servicemonitor',
        namespace: 'monitoring',
      },
      spec: {
        jobLabel: 'app',
        endpoints: [
          {
            port: 'http',
            path: 'metrics',
            interval: '30s',
          },
        ],
        selector: {
          matchLabels: {
            app: 'prometheus-cloudwatch-exporter',
          },
        },
      },
      },
      serviceMonitorMyNamespace: {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'ServiceMonitor',
        metadata: {
          name: 'project-servicemonitor',
          namespace: 'pfe-project',
        },
        spec: {
          jobLabel: 'app',
          endpoints: [
            {
              port: 'http-metrics',
            },
          ],
          selector: {
            matchLabels: {
              monitored: 'true',
            },
          },
        },
      },
    },
  
  };

  { ['00namespace-' + name]: kp.kubePrometheus[name] for name in std.objectFields(kp.kubePrometheus) } +
  { ['0prometheus-operator-' + name]: kp.prometheusOperator[name] for name in std.objectFields(kp.prometheusOperator) } +
  { ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
  { ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
  { ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
  { ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
  { ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
  { ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) } +
  { ['ingress-' + name]: kp.ingress[name] for name in std.objectFields(kp.ingress) }

