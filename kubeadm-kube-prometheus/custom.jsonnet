  local grafana = import 'grafonnet/grafana.libsonnet';
  local dashboard = grafana.dashboard;
  local row = grafana.row;
  local prometheus = grafana.prometheus;
  local template = grafana.template;
  local graphPanel = grafana.graphPanel;
  local kp = (import 'kube-prometheus/kube-prometheus.libsonnet') + (import 'kube-prometheus/kube-prometheus-kubeadm.libsonnet') + (import 'kube-prometheus/kube-prometheus-node-ports.libsonnet') + (import 'kube-prometheus/kube-prometheus-anti-affinity.libsonnet') {
    _config+:: {
      namespace: 'monitoring',
      prometheus+:: {
        namespaces+: ['pfe-project','amazon-cloudwatch'],
      },
      grafana+:: {
        config: { // http://docs.grafana.org/installation/configuration/
          sections: {
            "auth.anonymous": {enabled: true},
          },
        },
      },

    },
    grafana+:: {
      dashboards+:: {
        'my-dashboard.json':
          dashboard.new('My Dashboard')
          .addTemplate(
            {
              current: {
                text: 'Prometheus',
                value: 'Prometheus',
              },
              hide: 0,
              label: null,
              name: 'datasource',
              options: [],
              query: 'prometheus',
              refresh: 1,
              regex: '',
              type: 'datasource',
            },
          )
          .addRow(
            row.new()
            .addPanel(graphPanel.new('My Panel', span=6, datasource='$datasource')
                      .addTarget(prometheus.target('vector(1)')))
          ),
      },
    },
    prometheus+:: {
      serviceMonitorMyNamespace: {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'ServiceMonitor',
        metadata: {
          name: 'project-servicemonitor',
          namespace: 'pfe-project',
        },
        spec: {
          jobLabel: 'app',
          endpoints: [
            {
              port: 'http-metrics',
            },
          ],
          selector: {
            matchLabels: {
              monitored: 'true',
            },
          },
        },
      },
    },
  
  };

  { ['00namespace-' + name]: kp.kubePrometheus[name] for name in std.objectFields(kp.kubePrometheus) } +
  { ['0prometheus-operator-' + name]: kp.prometheusOperator[name] for name in std.objectFields(kp.prometheusOperator) } +
  { ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
  { ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
  { ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
  { ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
  { ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
  { ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) } 
  