{{/*  default annotation frontend ingress */}}
{{- define "ingress.annotations" }}
    kubernetes.io/ingress.class: alb
    alb.ingress.kubernetes.io/scheme: {{ .Values.ingress.scheme }}
    alb.ingress.kubernetes.io/certificate-arn: {{ .Values.ingress.certificateArn }}
    alb.ingress.kubernetes.io/listen-ports: '[{"HTTP": 80}, {"HTTPS":443}]'
    alb.ingress.kubernetes.io/actions.ssl-redirect: '{"Type": "redirect", "RedirectConfig": { "Protocol": "HTTPS", "Port": "443", "StatusCode": "HTTP_301"}}'
    alb.ingress.kubernetes.io/group.name: k8s-monitoring
    alb.ingress.kubernetes.io/tags: auto-delete=no,auto-stop=no
{{- end }}
{{/*   frontend ingress rules */}}
{{ define "ingress.rules" }}
    - host: "k8s.pfe-project-graduation.cloud"
      http:
        paths:
          - path: /*
            backend:
              serviceName: ssl-redirect
              servicePort: use-annotation
          - path: /*
            backend:
              serviceName: {{ .Values.serviceFrontEnd.name }}
              servicePort: {{ .Values.serviceFrontEnd.port }}
{{- end }}