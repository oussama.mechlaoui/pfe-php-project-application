<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
	        global $database;
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "agence")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			$sql = "select * from categories";
			$requete = "select * from offres";
			//$requete =  "SELECT DISTINCT categorie, nom FROM pfe.categories, pfe.offres WHERE categories.id_cat = offres.id_cat and offres.etat = 'active'";
			$categories = Categorie::find_all_query($sql);
			$offres = Offre::find_all_query($requete);
			$mons = array(1 => "Janvier", 2 => "Fevrier", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "Juillet", 8 => "Aout", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Decembre");
            $date = getdate();
			$day = $date['mday'];
            $month = $date['mon'];
	        $year = $date['year'];
            $month_name = $mons[$month];
			
		    
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT espace - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">
	
	<link id="base-style" href="../style/css/style.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Chef Commercial' ;}
																				       else if ($user->profil == 'agence')
																					        { echo'Espace TT - '. $user->espace ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-table"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatag.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffag.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-list-ol"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="../vente/ajvente.php">Ajout Ventes</a>
                            </li>
                            <li>
                                <a href="../vente/listvente.php">Liste des Ventes</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                        <li>
                                <a href="suivi.php">Suivi Objectifs</a>
                            </li>
						</ul>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                       <li>
                                <a href="../statistiques/stats.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Objectifs</li>
                  <li class="active">Suivi des Objectifs</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <br> <br>
				<div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"> 
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i> Objectifs</h3>
                            </div>
                            <div class="panel-body">
              <form name="consventes" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
		
                        <fieldset class="form-group">
						<div class="col-xs-6">
						          <?php $this_page = "suivi.php" ; ?>
                                 <label for="exampleSelect1" id="sel" >Objectif par</label>
                                 <select class="form-control" name="objectif" id="sel1" onChange="window.location='suivi.php?objectif='+this.value">
								 <option value="">Autres</option>
								 <option value="Categorie">Categorie </option>
								 <option value="Offre">Offre </option>
                                 </select>
						</div>		 
                        </fieldset>
						
						<fieldset class="form-group">
						<div class="col-xs-6">
                                 <label for="exampleSelect1"><?php if(!empty($_GET['objectif'])) {echo $_GET['objectif'] ;} else {echo "Categorie/offre";} ?></label>
                                 <select class="form-control" name="choix" id="sel2" >
								 
								 <?php
								 if ($_GET['objectif'] == "Categorie"){
									 $cat = new Categorie();
									  echo'<option value="a">Toutes les categories </option>';
									 foreach($categories as $cat){
									 
                                       echo'<option value="'.$cat->categorie.'">'.$cat->categorie.'</option>' ;
								 }
								 }
								 else if ($_GET['objectif'] == "Offre"){
                                 $off = new Offre();
								   echo'<option value="b">Toutes les offres </option>';
								 foreach($offres as $off){
									 
                                       echo'<option value="'.$off->nom.'">'.$off->nom.'</option>' ;
								 }
								 }
									   ?>
                                 </select>
					    </div>
                        </fieldset>
						<fieldset class="form-group">
					                    <div class="col-xs-6">
                                              <label for="date-picker-2" class="control-label">Ann&eacute;e</label>
                                                    <select class="form-control" name="ans" id="sel1">
													<?php
													echo'<option value="'.$year.'">Ann&eacute;e Actuelle </option>';
                                                    for($i = 2016; $i < date("Y")+1; $i++){
	                                                echo '<option value="'.$i.'">'.$i.'</option>';
                                                    }
                                                    ?>
                                                   </select>
								        </div>
                                  </fieldset>
						
                        <div class="input-group-btn">
                <button class="btn btn-default" id="rech" name ="rechercher" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
              </form> <br>
			  <br>
			  <?php 
			  if (isset($_POST['rechercher'])){
				  if($_GET['objectif'] == "Categorie"){
					 if($_POST['choix'] == "a"){
					  $req = "select * from pfe.objectifs where DRT='".$user->DRT."' and date='".$_POST['ans']."' "	;			  
				  }
                   else{
			     $req = "select * from pfe.objectifs where categorie='".$_POST['choix']."' and DRT='".$user->DRT."' and date='".$_POST['ans']."' ";
				   }				  
				  }
			  else if ($_GET['objectif'] == "Offre"){
				  if ($_POST['choix'] == "b"){
					  $req = "select * from pfe.objectifs where DRT='".$user->DRT."' and date='".$_POST['ans']."'";
				  }
				  else{
					   $req = "select * from pfe.objectifs where offre='".$_POST['choix']."' and DRT='".$user->DRT."' and date='".$_POST['ans']."' ";
				  }
			  }
				  $obj = Objectifs::find_all_query($req);
				  if($obj == null){
					  echo '<script language="Javascript">
                <!--
                alert("Objectif non trouv�");
				document.location.replace("suivi.php");
                // -->
				  </script>';}
				  else {
				  		 
			  	  							  
				  echo' <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-shopping-cart"></i>'; if ($_POST['choix'] == "a"){
									echo ' Ventes toutes categories';
								}
									else if ($_POST['choix'] == "b") {
										echo ' Ventes toutes offres';
									}
									else {
										echo ' Ventes '.$_POST['choix'];
									}
							echo'</h3>
							
                            </div>';
							
                            echo'<div class="panel-body">';
									$objectif = new Objectifs();
				    foreach($obj as $objectif){
		if(isset($_POST['ans']) == $year) {
						     /***************************************************Affichage this_year **********************/
						$req = "select SUM(quantite) as quantite from pfe.ventes where date between '".$year."-01-01' and '".$year."-06-30' and offre ='".$objectif->offre."' and espacett='".$user->espace."' and DRT='".$user->DRT."'";
						$req1 = "select SUM(quantite) as quantite from pfe.ventes where date between '".$year."-07-01' and '".$year."-12-31' and offre ='".$objectif->offre."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";
						$req2 = "select SUM(quantite) as quantite from pfe.ventes where date between '".$year."-".$month."-01' and '".$year."-".$month."-31' and offre ='".$objectif->offre."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";
						$semestre1 = Vente::find_all_query($req);
						$semestre2 = Vente::find_all_query($req1);
						$cemois = Vente::find_all_query($req2);
												

							  echo'<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header">
						<h2><i class="halflings-icon white align-justify"></i><span ></span>Ventes '.$objectif->offre.'</h2>
					</div>';
					echo'<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th></th>
									  <th>Semestre 1</th>
									  <th>Semestre 2</th>';
									  echo'<th>'.$month_name.'</th>' ;                                       
								 echo' </tr>
							  </thead>   
							  <tbody>';
								  echo'<tr>';
                                           echo'<td>Objectifs par region</td>';
                                           echo'<td>'.$objectif->objectifsregion1.'</td>';
										   echo'<td> '.$objectif->objectifsregion2.'</td>';
										   //$mois contient l'objectif d'une region par mois
										   if($month >6){
										   $mois = round($objectif->objectifsregion2/6);
										   }
										   else{
											  $mois = round($objectif->objectifsregion1/6);  
										   }
                                           echo'<td>'.$mois.'</td>';
                                           echo'</tr>';
										   
										   echo'<tr>';
                                           echo'<td>Objectifs par espace TT</td>';
										   //$mois1 contient l'objectif d'un espacett par mois (smestre2)
										   if($month >6){
										   $mois1 = round($objectif->objectifsespace2/6);
										   }
										   //$mois1 contient l'objectif d'un espacett par mois (smestre1)
										   else {
										   $mois1 = round($objectif->objectifsespace1/6);
										   }
                                           echo'<td>'.$objectif->objectifsespace1.'</td>';
										   echo'<td>'.$objectif->objectifsespace2.' </td>';
										   echo'<td>'.$mois1.'</td>';                                         
                                           echo'</tr>';
										   
										   
										   $sem1 = new Vente();
										   $sem2 = new Vente();
										   $moiss = new Vente();
					
										   echo'<tr>';
                                           echo'<td>Realisation</td>';
										   foreach($semestre1 as $sem1){
                                           echo'<td>'.$sem1->quantite.'</td>';
										   $s1 = $sem1->quantite;
										   }
										   foreach($semestre2 as $sem2){
										   echo'<td> '.$sem2->quantite.'</td>';
										   $s2 = $sem2->quantite;
										   }
										   foreach($cemois as $moiss){
                                           echo'<td>'.$moiss->quantite.'</td>';
										   $m = $moiss->quantite;
										   }
                                           echo'</tr>';
										   
										   
										   echo'<tr>';
                                           echo'<td>Taux de realisation</td>';
                                           echo'<td>'.round(($s1/$objectif->objectifsespace1)*100).' %</td>';
										   echo'<td>'.round(($s2/$objectif->objectifsespace2)*100).' %</td>';
                                           echo'<td>'.round(($m/$mois1)*100).' %</td>';
                                           echo'</tr>';
										   
		} else {
			     $req = "select SUM(quantite) as quantite from pfe.ventes where date between '".$_POST['ans']."-01-01' and '".$_POST['ans']."-06-30' and offre ='".$objectif->offre."' and espacett='".$user->espace."' and DRT='".$user->DRT."'";
						$req1 = "select SUM(quantite) as quantite from pfe.ventes where date between '".$_POST['ans']."-07-01' and '".$_POST['ans']."-12-31' and offre ='".$objectif->offre."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";

						$semestre1 = Vente::find_all_query($req);
						$semestre2 = Vente::find_all_query($req1);
												

							  echo'<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header">
						<h2><i class="halflings-icon white align-justify"></i><span ></span>Ventes '.$objectif->offre.'</h2>
					</div>';
					echo'<div class="box-content">
						<table class="table">
							  <thead>
								  <tr>
									  <th></th>
									  <th>Semestre 1</th>
									  <th>Semestre 2</th>';                                       
								 echo' </tr>
							  </thead>   
							  <tbody>';
								  echo'<tr>';
                                           echo'<td>Objectifs par region</td>';
                                           echo'<td>'.$objectif->objectifsregion1.'</td>';
										   echo'<td> '.$objectif->objectifsregion2.'</td>';
                                           echo'</tr>';
										   
										   echo'<tr>';
                                           echo'<td>Objectifs par espace TT</td>';
                                           echo'<td>'.$objectif->objectifsespace1.'</td>';
										   echo'<td>'.$objectif->objectifsespace2.' </td>';                                       
                                           echo'</tr>';
										   
										   $sem1 = new Vente();
										   $sem2 = new Vente();
					
										   echo'<tr>';
                                           echo'<td>Realisation</td>';
										   foreach($semestre1 as $sem1){
                                           echo'<td>'.$sem1->quantite.'</td>';
										   $s1 = $sem1->quantite;
										   }
										   foreach($semestre2 as $sem2){
										   echo'<td> '.$sem2->quantite.'</td>';
										   $s2 = $sem2->quantite;
										   }

                                           echo'</tr>';
										   
										   
										   echo'<tr>';
                                           echo'<td>Taux de realisation</td>';
                                           echo'<td>'.round(($s1/$objectif->objectifsespace1)*100).' %</td>';
										   echo'<td>'.round(($s2/$objectif->objectifsespace2)*100).' %</td>';
                                           echo'</tr>';
		}							   
				
					
							  echo'</tbody>
						 </table> '; 
					
					
					echo'</div>';
					
				echo'</div>';
					}
					}
					
				
					
				                      
                           echo' </div>
                        </div>
                    </div>
                </div>';
				  
			  
			  }
			  
			  			  
			  ?>
                            </div>
                        </div>
                    </div>
                </div>
             
            </div>
				
                <!-- /.row -->
				
                    
              
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../style/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="bootstrap-datepicker.de.js" charset="UTF-8"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>

	

</body>

</html>
