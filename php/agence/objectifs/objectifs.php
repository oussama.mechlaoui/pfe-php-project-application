<?php require_once("init.php"); ?>

<?php

Class Objectifs {
	
	public $id_objectif;
	public $objectifsespace1;
	public $objectifsespace2;
	public $objectifsregion1;
	public $objectifsregion2;
	public $categorie;
	public $offre;
	public $date;
	public $DRT;


	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_objectifs(){

global $database;
$requete = "INSERT INTO pfe.objectifs (objectifsespace1,objectifsregion1,categorie,offre,date, DRT) VALUES ('".$database->escape_string($this->objectifsespace1)."','".$database->escape_string($this->objectifsregion1)."','".$database->escape_string($this->categorie)."','".$database->escape_string($this->offre)."','".$database->escape_string($this->date)."','".$database->escape_string($this->DRT)."')" ;

if ($database->query($requete)){
	    $this->id_objectif = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_objectifs() {

global $database;
$requete = "UPDATE pfe.objectifs SET objectifsespace1 = '".$this->objectifsespace1."',objectifsespace2 = '".$this->objectifsespace2."', objectifsregion1 = '".$this->objectifsregion1."',objectifsregion2 = '".$this->objectifsregion2."', categorie = '".$database->escape_string($this->categorie)."', offre = '".$database->escape_string($this->offre)."',periode= '".$database->escape_string($this->periode)."' ,date = '".$database->escape_string($this->date)."' , DRT='".$database->escape_string($this->DRT)."' WHERE id_objectif ='".$this->id_objectif."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function update_objectifssem2() {

global $database;
$requete = "UPDATE pfe.objectifs SET objectifsespace2 = '".$this->objectifsespace2."', objectifsregion2 = '".$this->objectifsregion2."' WHERE id_objectif ='".$this->id_objectif."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_objectifs(){
	
global $database;
$requete = "DELETE FROM pfe.objectifs WHERE objectifs.id_objectif ='".$this->id_objectif."'"	;
$sql= "ALTER TABLE pfe.objectifs AUTO_INCREMENT = ".$this->id_objectif.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

