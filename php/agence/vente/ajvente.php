<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
	        global $database;
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "agence")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			$sql = "select * from categories";
			//$requete =  "SELECT DISTINCT categorie, nom FROM pfe.categories, pfe.offres WHERE categories.id_cat = offres.id_cat and offres.etat = 'active'";
			$categories = Categorie::find_all_query($sql);
			
		    
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT espace - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Chef Commercial' ;}
																				       else if ($user->profil == 'agence')
																					        { echo'Espace TT - '. $user->espace ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatag.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffag.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajvente.php">Ajout Ventes</a>
                            </li>
                            <li>
                                <a href="listvente.php">Liste des Ventes</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                        <li>
                                <a href="../objectifs/suivi.php">Suivi Objectifs</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                       <li>
                                <a href="../statistiques/stats.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Accueil</a></li>
                  <li class="active" >Ventes</li>
                  <li class="active">Ajout Ventes</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <br> <br>
				
                <!-- /.row -->
				
                    <div class="row">
					<?php  
					$cat = new Categorie();
					echo'<form name="ajvente"  method=POST>';
					foreach($categories as $cat) { 
					
                    echo'<div class="col-lg-12">
					
                        <div class="panel panel-default">';
					
                            echo'<div class="panel-heading">';
							                
                                echo'<h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>'.$cat->categorie.'</h3>';
								echo'</div>'; 
                             echo'<div class="panel-body">';
					$requete = "select * from offres where etat ='active' and id_cat="."'".$cat->id_cat."'";
					$offres = Offre::find_all_query($requete);
					$off = new Offre();
					foreach($offres as $off) {
                       echo' <fieldset class="form-group">
					          <div class="col-xs-4">';
                                 echo'<label for="exampleSelect1">'.$off->nom.' </label>
					<input class="form-control" type="text" name="'.$off->id_offre.'" >
								 </div>
					</fieldset>'; 
					if (isset($_POST['ajouter'])){
					if(!empty($_POST[$off->id_offre])){
					  $vente = new Vente();
					  $vente->offre = $off->nom;
					  $vente->categorie= $cat->categorie;	
					  $vente->quantite = $_POST[$off->id_offre];	
                      $vente->espacett = $user->espace;					
					  $vente->date = date("Y-m-d");					  
					  $vente->DRT = $user->DRT;
					  $vente->add_vente();	
					  
					}
					}
					}					                             
					 echo'</div>
                        </div> ';
                    echo'</div>';
					}
					
					 echo'<div class="col-md-12 center-block">
						<button type="submit" name="ajouter" class="btn btn-primary">Ajouter</button>
						<button type="submit" name="annuler" class="btn btn-primary">Annulation</button>
						</div>
					</form>';
					?>
						
             
			  </div> 
              
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>

</body>

</html>
