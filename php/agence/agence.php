<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "agence")
                        {
                                header('location: ../'.$user->profil.'/'.$user->profil.'.php');
                        }
			$sql = "select * from categories";
			$categories = Categorie::find_all_query($sql);
			$mons = array(1 => "Janvier", 2 => "Fevrier", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "Juillet", 8 => "Aout", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Decembre");
            $date = getdate();
            $month = $date['mon'];
	        $year = $date['year'];
            $month_name = $mons[$month];
			
			// Bar Chart pour categorie/offres/realisation par Mois actuel
			
			$tablebar = array();
            $tablebar['cols'] = array(
            //Labels for the chart, these represent the column titles
			array('id' => '', 'label' => 'Offres', 'type' => 'string'),
            array('id' => '', 'label' => 'Realisations', 'type' => 'number')
             ); 
			$tabbar = array();
			    foreach($categories as $catgbar){
				$reqoff = "select * from offres where id_cat ='".$catgbar->id_cat."' and etat='active'";
				$offbar = Offre::find_all_query($reqoff);
				foreach($offbar as $offbar1){
				$reqbar = "select offre,categorie, SUM(quantite) as quantite from pfe.ventes where date between '".$year."-".$month."-01' and '".$year."-".$month."-31' and offre='".$offbar1->nom."' and categorie ='".$catgbar->categorie."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";
				$bar = Vente::find_all_query($reqbar);
				$tempbar = array();
				$tempbar[] = array('v' => (string)$offbar1->nom);
				foreach($bar as $bar1){
                $tempbar[] = array('v' => (int)$bar1->quantite); 
                $tabbar[] = array('c' => $tempbar);				
				}
				}
				}
			$tablebar['rows'] = $tabbar;
			$jsontablebar = json_encode($tablebar, true); 
			
			// pie char chart pour le mois actuel 
            $table = array();
            $table['cols'] = array(
            //Labels for the chart, these represent the column titles
            array('id' => '', 'label' => 'Categories', 'type' => 'string'),
            array('id' => '', 'label' => 'Realisations', 'type' => 'number')
             ); 
			$tab = array();
			    foreach($categories as $catg){
				$req2 = "select categorie, SUM(quantite) as quantite from pfe.ventes where date between '".$year."-".$month."-01' and '".$year."-".$month."-31' and categorie ='".$catg->categorie."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";
				$cemois = Vente::find_all_query($req2);
				foreach($cemois as $m){
                $temp = array();
                $temp[] = array('v' => (string)$m->categorie);
                $temp[] = array('v' => (int)$m->quantite); 
                $tab[] = array('c' => $temp);				
				}
				}
			$table['rows'] = $tab;
			$jsontable = json_encode($table, true);
		    
}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT espace - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Bar Chart -->
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart', 'bar']});
          google.charts.setOnLoadCallback(drawChartBAR);
	  google.charts.setOnLoadCallback(drawChartPIE);
      function drawChartBAR() {
        var data = new google.visualization.DataTable(<?=$jsontablebar?>);
        var options = {
          chart: {
            title: 'Performance des offres - <?php echo ' mois en cours: ' .$month_name. ' - année en cours: ' .$year; ?>',
            subtitle: 'Offres et Realisations',
          }
        };
		        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, options);
	  }
	  
	  function drawChartPIE() {
	  
		var data1 = new google.visualization.DataTable(<?=$jsontable?>);
      var options1 = {
           title: ' Realisations/Categories, Mois : <?php echo $month_name.' '.$year; ?>. ',
          is3D: 'true',
          width: 450,
          height: 343
        };
		
		var chart1 = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart1.draw(data1, options1);
		
	  } 
      
    </script>
	
	 
	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Chef Commercial' ;}
																				       else if ($user->profil == 'agence')
																					        { echo'Espace TT - '. $user->espace ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo $user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="consultation/conscatag.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="consultation/consoffag.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="vente/ajvente.php">Ajout Vente</a>
                            </li>
                            <li>
                                <a href="vente/listvente.php">Liste des Ventes</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                        <li>
                                <a href="objectifs/suivi.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                       <li>
                                <a href="statistiques/stats.php">Stats</a>
                            </li>
                        </ul>
                    </li>					
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Accueil <small>Aperçu des Statisques</small>
                        </h1>
                    </div>
                </div>
				
                <!-- /.row -->


                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Realisations/Offres</h3>
                            </div>
                            <div class="panel-body">
                               <div id="columnchart_material" style="width: 1000px; height: 400px;"></div> <br> </br>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                   <div class="col-lg-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Realisations/Categories</h3>
                            </div>
                            <div class="panel-body">
                               <div id="chart_div" style="width: 450px; height: 343px; float:left;"></div>
                                <div class="text-right">
                                    <a href="statistiques/stats.php">Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <div class="col-lg-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Objectifs/offres: <?php echo $month_name.' '.$year; ?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Offres</th>
                                                <th>Objectifs</th>
                                                <th>Realisations</th>
                                                <th>Taux de Realisations</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php
										 $reqoffre = "select * from offres limit 10";
										 $offr = Offre::find_all_query($reqoffre);
										 foreach ($offr as $offag){
										 $reqobj = "select * from pfe.objectifs where offre='".$offag->nom."' and DRT='".$user->DRT."' and date='".$year."' ";	
										 $objectifs = Objectifs::find_all_query($reqobj);
										 foreach($objectifs as $objectif){
										 $reqvente = "select SUM(quantite) as quantite from pfe.ventes where date between '".$year."-".$month."-01' and '".$year."-".$month."-31' and offre ='".$offag->nom."' and espacett='".$user->espace."' and DRT='".$user->DRT."' ";
                                         $cemois = Vente::find_all_query($reqvente);
										 echo'<tr>';
                                         echo'<td>'.$offag->nom.'</td>';
										 if($month > 6){
                                         echo'<td>'.round($objectif->objectifsespace2/6).'</td>';
										 $taux = round($objectif->objectifsespace2/6);
										 }
										 else {
										 echo'<td>'.round($objectif->objectifsespace1/6).'</td>';	
										 $taux = round($objectif->objectifsespace1/6);
										 }
										 foreach($cemois as $moiss){
                                         echo'<td>'.$moiss->quantite.'</td>';
										    $m = $moiss->quantite;
										   }
                                         echo'<td>'.round(($m/$taux)*100).' %</td> 
                                            </tr> ';                                       
										}
										}										 
                                            
                                        ?>    
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a href="objectifs/suivi.php">Details <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>

</body>

</html>
