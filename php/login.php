<?php include("init.php"); ?>
<?php
if ($session->signed_in()){
$user = Utilisateur::find_user_by_id($session->user_id) ;
if ($user != null){
header('location: '.$user->profil.'/'.$user->profil.'.php');
}}
if (isset($_POST['submit'])){
	
	$login = trim($_POST['login']);
	$password = trim($_POST['password']);
	
	$user = User::verify($login,$password);
	
	if ($user) {
		
		$session->login($user);
	
	} else {
		
		$msg = "Erreur d'authentification – échec de l'authentification. Veuillez vérifier votre Login et votre Mot de passe." ;
		
	}
	
} else {
	$login = "";
	$password = "";
}

	
?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="style/css/login.css" />
</head>
<body>
<div class="container">
<div class="login">
<section class="loginform cf">
<form name="login" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
    <ul>
        <li><label for="utilisateur">Login</label>
        <input type="text" name="login" placeholder="Username or Email" required></li>
        <li><label for="password">Password</label>
        <input type="password" name="password" placeholder="Password" required></li>
        <li>
        <input type="submit" name="submit" value="Login"></li>
    </ul>
</form>
<p> <?php  if (isset($_POST['submit']) && !$user) { unset($_POST['submit']); echo $msg  ;} ?> </p>
</section>
</div>
</div>

				
</body>
</html>
