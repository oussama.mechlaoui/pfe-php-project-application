<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "admin")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			if (isset($_POST['inserer'])){
				global $logo;
				$file = $_FILES['upload'];
				$logo = Upload::checklogo($file);
				
				echo '<script language="Javascript">
                <!--
                alert("Logo inséré avec succès");
				
                // -->
                </script>';
			}
		 
			
}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT Admin - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<a class="navbar-brand1" ><img src="images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Chef Commercial' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-table"></i> Gestion Catégories <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="../categorie/ajcat.php">Ajout Categorie</a>
                            </li>
                            <li>
                                <a href="../categorie/conscat.php">Consultation Categories</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-edit"></i> Gestion des Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                        <li>
                                <a href="../offre/ajoffre.php">Ajout Offre</a>
                            </li>
                            <li>
                                <a href="../offre/consoffre.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo6"><i class="fa fa-fw fa-edit"></i> Gestion des Forfaits <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo6" class="collapse">
                        <li>
                                <a href="../forfait/ajforfait.php">Ajout Forfait</a>
                            </li>
                            <li>
                                <a href="../forfait/consforfait.php">Consultation Forfaits</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-user"></i> Gestion des Utilisateurs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                        <li>
                                <a href="../utilisateur/ajuti.php">Ajout Utilisateur</a>
                            </li>
                            <li>
                                <a href="../utilisateur/consuti.php">Consultation Utilisateurs</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-bar-chart-o"></i> Gestion des Espace TT <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                       <li>
                                <a href="../espace/ajespace.php">Ajout Espace TT</a>
                            </li>
                            <li>
                                <a href="../espace/consespace.php">Consultation Espaces TT</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-wrench"></i> Paramétrage <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                        <li>
                                <a href="logo.php">Logo</a>
                            </li>
                           
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Parametrage</li>
                  <li class="active">Logo</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
            <br> <br>
                <!-- /.row -->
                    

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Logo</h3>
                            </div>
                            <div class="panel-body">
                            <form name="upload"  method="post" enctype="multipart/form-data">   
                                <fieldset class="form-group">
                                   <label for="exampleInputFile">Upload Logo</label>
                                   <input type="file" name="upload" class="form-control-file" id="exampleInputFile">
                                   
                                </fieldset>
                            <button type="submit" name="inserer" class="btn btn-primary">Ajouter</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>

</body>

</html>
