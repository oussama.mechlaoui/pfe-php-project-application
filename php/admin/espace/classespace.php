<?php require_once("init.php"); ?>

<?php

Class Espace {
	
	public $id_espace;
	public $espace;
	public $DRT;
	public $description;

public static function find_espace_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.espacett where id_espace="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_espace(){

global $database;
$requete = "INSERT INTO pfe.espacett (espace, DRT ,description)VALUES('".$database->escape_string($this->espace)."', '".$database->escape_string($this->DRT)."' ,'".$database->escape_string($this->description)."')" ;

if ($database->query($requete)){
	    $this->id_espace = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_espace() {

global $database;
$requete = "UPDATE pfe.espacett SET espace = '".$database->escape_string($this->espace)."', DRT='".$database->escape_string($this->DRT)."' ,description = '".$database->escape_string($this->description)."' WHERE id_espace ='".$database->escape_string($this->id_espace)."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_espace(){
	
global $database;
$requete = "DELETE FROM pfe.espacett WHERE id_espace ='".$database->escape_string($this->id_espace)."'"	;
$sql= "ALTER TABLE pfe.espacett AUTO_INCREMENT = ".$this->id_espace.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

