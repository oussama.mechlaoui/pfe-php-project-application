<?php require_once("init.php"); ?>

<?php

Class Categorie {
	
	public $id_cat;
	public $categorie;
	public $description;

public static function find_cat_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.categories where id_cat="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_categorie(){

global $database;
$requete = "INSERT INTO pfe.categories (categorie, description) VALUES ('".$database->escape_string($this->categorie)."','".$database->escape_string($this->description)."')" ;

if ($database->query($requete)){
	    $this->id_cat = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_categorie() {

global $database;
$requete = "UPDATE pfe.categories SET categorie = '".$this->categorie."', description = '".$database->escape_string($this->description)."' WHERE id_cat ='".$this->id_cat."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_categorie(){
	
global $database;
$requete = "DELETE FROM pfe.categories WHERE categories.id_cat ='".$this->id_cat."'"	;
$sql= "ALTER TABLE pfe.categories AUTO_INCREMENT = ".$this->id_cat.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

