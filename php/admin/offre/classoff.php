<?php require_once("init.php"); ?>

<?php

Class Offre {
	
	public $id_offre;
	public $id_cat;
	public $nom;
	public $description;
	public $etat;

public static function find_offre_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.offres where id_offre="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_offre(){

global $database;
$requete = "INSERT INTO pfe.offres (id_cat,nom,description,etat)VALUES('".$database->escape_string($this->id_cat)."','".$database->escape_string($this->nom)."','".$database->escape_string($this->description)."','".$database->escape_string($this->etat)."')" ;

if ($database->query($requete)){
	    $this->id_offre = $database->the_insert_id();
		return true;
	}else {
		return false;
	}
	
}

public function update_offre() {

global $database;
$requete = "UPDATE pfe.offres SET id_cat = '".$database->escape_string($this->id_cat)."', nom = '".$database->escape_string($this->nom)."' , description = '".$database->escape_string($this->description)."' , etat = '".$database->escape_string($this->etat)."' WHERE id_offre ='".$database->escape_string($this->id_offre)."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_offre(){
	
global $database;
$requete = "DELETE FROM pfe.offres WHERE offres.id_offre ='".$this->id_offre."'"	;
$sql= "ALTER TABLE pfe.offres AUTO_INCREMENT = ".$this->id_offre.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>
