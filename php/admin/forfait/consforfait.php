<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
			                        if ($user->profil != "admin")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;
								
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
	 <link href="../style/css/agenda.css" rel="stylesheet">
	 	<link href="../style/css/halflings.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
		

	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Chef Commercial' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-table"></i> Gestion des Cat&eacute;gories <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="../categorie/ajcat.php">Ajout Categorie</a>
                            </li>
                            <li>
                                <a href="../categorie/conscat.php">Consultation Categories</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-edit"></i> Gestion des Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                        <li>
                                <a href="../offre/ajoffre.php">Ajout Offre</a>
                            </li>
                            <li>
                                <a href="../offre/consoffre.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo6"><i class="fa fa-fw fa-edit"></i> Gestion des Forfaits <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo6" class="collapse">
                        <li>
                                <a href="ajforfait.php">Ajout Forfait</a>
                            </li>
                            <li>
                                <a href="consforfait.php">Consultation Forfaits</a>
                            </li>
                        </ul>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-user"></i> Gestion des Utilisateurs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
                        <li>
                                <a href="../utilisateur/ajuti.php">Ajout Utilisateur</a>
                            </li>
                            <li>
                                <a href="../utilisateur/consuti.php">Consultation Utilisateurs</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-bar-chart-o"></i> Gestion des Espace TT <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                       <li>
                                <a href="../espace/ajespace.php">Ajout Espace TT</a>
                            </li>
                            <li>
                                <a href="../espace/consespace.php">Consultation Espaces TT</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-wrench"></i> Param&eacute;trage <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                        <li>
                                <a href="../parametrer/logo.php">Logo</a>
                            </li>
                           
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Gestion des Forfaits</li>
                  <li class="active">Consultation Forfaits</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
             <br> <br>
                
				 <div class="row">
					
                    <div class="col-lg-12">
                        <div class="panel panel-default">					
                            <div class="panel-heading">		                
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Consultation Forfaits</h3>
								</div> 
                             <div class="panel-body">
                                    <div class="table-responsive">
									<button type="button" id="exportButton" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                                    <table id="myTable" class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Forfait</th>
                                                <th>Categorie</th>
                                                <th>Offre</th>
												<th>Description</th>
												<th>Etat</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php 
										$rqforfaits = "select * from forfait" ;
							            $forfaits = Forfait::find_all_query($rqforfaits);
							   $for = new Forfait();
							   foreach ($forfaits as $for){
                                           echo'<tr>';
                                           echo'<td>'.$for->forfait.'</td>';
                                           echo'<td>'.$for->categorie.'</td>';
										   echo'<td>'.$for->offre.'</td>';
										   echo'<td>'.$for->description.'</td>';
										   echo'<td>'.$for->etat.'</td>';
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modforfait.php?id='.$for->id_forfait.'&amp;forfait='.$for->forfait.'&amp;categorie='.$for->categorie.'&amp;offre='.$for->offre.'&amp;des='.$for->description.'&amp;etat='.$for->etat.'">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppforfait.php?id='.$for->id_forfait.'">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										   echo'</tr>';
										} 
											?>
                                        </tbody>
                                    </table>

										 </div>
										 
										   
									

            </div>
			</div>
                                

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src='../style/js/footable.js'></script>
	<script type="text/javascript" src='../style/js/footable.min.js'></script>
	
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.forfait.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>


	
	
	<script>
$(document).ready(function(){
    $('#myTable').dataTable();
	document.getElementById("myTable").style.width = "100%";

	
});
</script>

<script>
$("#exportButton").hover(
function () {
  
   $(this).text("PDF");
});
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButton").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#myTable",
                schema: {
                    type: "table",
                    fields: {
                        Forfait: {type : String},
						Categorie: {type : String},
						Offre: {type : String},
						Description: {type: String},
						Etat: {type: String}
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "Forfait", title: "Forfait", width: 140 },
						{ field: "Categorie", title: "Categorie", width: 100 },
						{ field: "Offre", title: "Offre", width: 100 },
						{ field: "Description", title: "Description", width: 120 },
						{ field: "Etat", title: "Etat", width: 120 }
                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "Liste_des_Forfaits"
                });
            });
        });
    });
</script>
	

	
	

</body>

</html>

