<?php require_once("init.php"); ?>

<?php

Class Forfait {
	
	public $id_forfait;
	public $forfait;
	public $categorie;
	public $offre;
	public $description;
	public $etat;

public static function find_forfait_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.forfait where id_forfait="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_forfait(){

global $database;
$requete = "INSERT INTO pfe.forfait (forfait,categorie,offre,description,etat)VALUES('".$database->escape_string($this->forfait)."','".$database->escape_string($this->categorie)."','".$database->escape_string($this->offre)."','".$database->escape_string($this->description)."','".$database->escape_string($this->etat)."')" ;

if ($database->query($requete)){
	    $this->id_forfait = $database->the_insert_id();
		return true;
	}else {
		return false;
	}
	
}

public function update_forfait() {

global $database;
$requete = "UPDATE pfe.forfait SET forfait = '".$database->escape_string($this->forfait)."', categorie = '".$database->escape_string($this->categorie)."' , offre = '".$database->escape_string($this->offre)."' , description = '".$database->escape_string($this->description)."' , etat = '".$database->escape_string($this->etat)."' WHERE id_forfait ='".$database->escape_string($this->id_forfait)."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_forfait(){
	
global $database;
$requete = "DELETE FROM pfe.forfait WHERE forfait.id_forfait ='".$this->id_forfait."'"	;
$sql= "ALTER TABLE pfe.forfait AUTO_INCREMENT = ".$this->id_forfait.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>
