<?php require_once("init.php"); ?>

<?php

Class Utilisateur {
	
	public $id;
	public $login;
	public $password;
	public $profil;
	public $nom;
	public $prenom;
	public $espace;
	public $DRT;

public static function find_user_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.utilisateur where id="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_user(){

global $database;
$requete = "INSERT INTO pfe.utilisateur (login,password,profil,nom,prenom,espace,DRT) VALUES ('".$database->escape_string($this->login)."', '".$database->escape_string($this->password)."', '".$database->escape_string($this->profil)."', '".$database->escape_string($this->nom)."', '".$database->escape_string($this->prenom)."', '".$database->escape_string($this->espace)."', '".$database->escape_string($this->DRT)."')" ;

if ($database->query($requete)){
	    $this->id = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_user() {

global $database;
$requete = "UPDATE pfe.utilisateur SET login = '".$database->escape_string($this->login)."', password = '".$database->escape_string($this->password)."', profil = '".$database->escape_string($this->profil)."', nom = '".$database->escape_string($this->nom)."', prenom = '".$database->escape_string($this->prenom)."', espace = '".$database->escape_string($this->espace)."', DRT='".$database->escape_string($this->DRT)."' WHERE id ='".$this->id."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }

}

public function delete_user(){
	
global $database;
$requete = "DELETE FROM pfe.utilisateur WHERE utilisateur.id ='".$this->id."'"	;
$sql= "ALTER TABLE pfe.utilisateur AUTO_INCREMENT = ".$this->id.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>