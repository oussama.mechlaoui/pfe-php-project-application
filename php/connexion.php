﻿<?php

require_once("config.php");

Class Database {
	
public $cnx;	

function __construct () {
	
	$this->open_db();
}

public function open_db () {
	
	$this->cnx = mysqli_connect($_SERVER['DB_HOST'],$_SERVER['DB_USERNAME'],$_SERVER['DB_PWD'],$_SERVER['DB_NAME']);
	if (mysqli_connect_errno()) {
		die("Pas de connexion à la base de données " . mysqli_error());
	}
	
}

public function query($requete){
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);	
	try {
		$RS = mysqli_query($this->cnx, $requete);
                return $RS;
        }catch (mysqli_sql_exception $e) {
      		throw $e;
   	} 
	//if (!$RS) {
	
//die ("requete invalide");
	//}
	//return $RS;
	
}

public function escape_string($strings) {
	
	$string = mysqli_real_escape_string($this->cnx, $strings);
	return $string;
}

public function the_insert_id() {
	
	return mysqli_insert_id($this->cnx);
}



}

$database = new Database();

?>
