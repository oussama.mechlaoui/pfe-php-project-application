﻿<?php


Class Database {
	
public $cnx;	
public $dbname="pfe";
public $user="root";
public $pass="root";
public $dsn ="mysql:dbname=pfe;host=127.0.0.1;port=3306;charset=utf8";

function __construct () {
	
	$this->open_db();
}

public function open_db () {
        try{
                $this->cnx = new \PDO($this->dsn,$this->user,$this->pass) ;
                $this->cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                echo "Connected successfully"; 
        }
        catch (PDOException $e)
        {
           	print "Pas de connexion à la base de données " . $e->getMessage() . "<br/>";
           	die();
        }
	
}

public function query($requete){
	$RS = $this->cnx->query($requete);
	if (!$RS) {
	die ("requete invalide");
	}
	return $RS;
	
}


public function the_insert_id() {
	
	return $this->cnx->lastInsertId();
}



}

$database = new Database();

?>
