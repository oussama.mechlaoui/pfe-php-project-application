<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;
			
			

			

}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
	 <link href="../style/css/agenda.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
		

	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Gestion Clients Affaires</li>
				  <li><a href="infclient.php">Consultation Client Affaires</a></li>
                  <li class="active">Calendrier des Actions</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
               <br> <br>
                
				 <div class="row">
					
                    <div class="col-lg-12">
                        <div class="panel panel-default">					
                            <div class="panel-heading">		                
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Calendrier des Actions</h3>
								</div> 
                             <div class="panel-body">
							 
							 <div class="container">
							 
    <div class="agenda">
        <div class="table-responsive">
		 <button type="button" id="exportButton" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
            <table id="action" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Commentaire</th>
						<th>CC</th>
						<th>Interlocuteur</th>
						<th>Client</th>
				
                    </tr>
                </thead>
                <tbody>
                    <!-- Single event in a single day -->
					<?php
					  $requete = "SELECT * FROM actions WHERE matfis='".$_GET['matis']."' ORDER BY date DESC";
					  $action = Actions::find_all_query($requete);
					  foreach($action as $act) {
						   $mons = array("Jan" => "Janvier", "Feb" => "Fevrier", "Mar" => "Mars", "Apr" => "Avril", "May" => "Mai", "Jun" => "Juin", "Jul" => "Juillet", "Aug" => "Aout", "Sept" => "Septembre", "Oct" => "Octobre", "Nov" => "Novembre", "Dec" => "Decembre");
						     $jours = array( 1 => "Lundi", 2 => "Mardi", 3 => "Mercredi", 4 => "Jeudi", 5 => "Vendredi", 6 => "Samedi", 7 => "Dimanche",);								
                                    
                                    $date = new DateTime($act->date);
                               
       								  $day= $date->format('j');
                                      $month = $date->format('M');
									  $year = $date->format('Y');
									  $this_month = $mons[$month];
									  $name = $date->format('N');
									  $this_day = $jours[$name];
									 
						 echo'   <tr>
                        <td class="agenda-date" class="active" rowspan="1"> <div class="dayofmonth">'.$day.'</div>  <div class="dayofweek">'.$this_day.'</div> <div class="shortdate text-muted">'.$this_month.', '.$year.'</div> </td>
                        <td>'.$act->act.'</td>
                        <td class="agenda-events"> <div class="agenda-event">'.$act->commentaire.' </div> </td>';
						$request = "select * from clientele where matricule = '".$act->matricule."' " ;
						$Clientele = Clientele::find_all_query($request);
						foreach ($Clientele as $cliente){
						echo'<td>'.$act->matricule.' '.$cliente->nom.' '.$cliente->prenom.' </td>';
						}
						$request1 = "select * from interlocuteur where identite ='".$_GET['cin_int']."'";
						$int = Interlocuteur::find_all_query($request1);
						foreach ($int as $inter) {
						echo'<td>'.$_GET['cin_int'].' '.$inter->nom.' '.$inter->prenom.'</td>';
						}
						$request2 = "select * from clients where matfis='".$act->matfis."'";
						$charge = Client::find_all_query($request2);
						foreach ($charge as $cc) {
						echo'<td> '.$act->matfis.' '.$cc->rs.' '.$cc->rc.'</td>';
						}
                    echo'</tr>';
					
						 
                      }						 
					  
					?>
                  
                </tbody>
            </table>
        </div>
    </div>
</div>

                                     
					         </div>
										
										
										

	
            </div>
            </div>
			</div>
                                

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src='../style/js/footable.js'></script>
	<script type="text/javascript" src='../style/js/footable.min.js'></script>
	
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.calendar.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>

<script>
$(document).ready(function(){
    $('#action').dataTable();



	
});
</script>

	<script>
$("#exportButton").hover(
function () {
  
   $(this).text("PDF");
});
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButton").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#action",
                schema: {
                    type: "table",
                    fields: {
                        Date: {type : String},
						Action: {type : String},
						Commentaire: {type : String},
						CC: {type: String},
						Interlocuteur: {type: String},
						Client: {type: String}
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "Date", title: "Date", width: 140 },
						{ field: "Action", title: "Action", width: 100 },
						{ field: "Commentaire", title: "Commentaire", width: 100 },
						{ field: "CC", title: "Charge Clientele", width: 120 },
						{ field: "Interlocuteur", title: "Interlocuteur", width: 120 },
						{ field: "Client", title: "Client Affaires", width: 120 }
                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "ListedesActions"
                });
            });
        });
    });
</script>
	
	

</body>

</html>

