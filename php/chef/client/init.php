<?php

require_once("../../function.php");
require_once("../../config.php");
require_once("../../connexion.php");
require_once("../../session.php");
require_once("../../user.php");
require_once("../../admin/categorie/classcat.php");
require_once("../../admin/offre/classoff.php");
require_once("../../admin/utilisateur/classuti.php");
require_once("../../admin/espace/classespace.php");
require_once("../../admin/parametrer/upload.php");
require_once("../../agence/vente/vente.php");
require_once("../../agence/objectifs/objectifs.php");
require_once("client.php");
require_once("clientele.php");
require_once("interlocuteur.php");
require_once("actions.php");
require_once("contrats.php");
require_once("souscontrat.php");
require_once("../../admin/forfait/classforfait.php");


?>

