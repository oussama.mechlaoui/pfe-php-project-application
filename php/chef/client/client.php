<?php require_once("init.php"); ?>

<?php

Class Client {
	
	public $matfis;
	public $mat;
    public $rs;
	public $rc;
	public $adresse;
	public $gouv;
	public $cp;
	public $activite;
	public $DRT;
	

	

public static function find_clients_by_matricule($mat){
global $database;
$requete = "SELECT * FROM pfe.clients where matfis="."'".$mat."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_client(){

global $database;
$requete = "INSERT INTO pfe.clients (rs, rc ,matfis,mat ,adresse,gouv,cp,activite,DRT) VALUES ('".$database->escape_string($this->rs)."', '".$database->escape_string($this->rc)."' ,'".$database->escape_string($this->matfis)."', '".$database->escape_string($this->mat)."', '".$database->escape_string($this->adresse)."','".$database->escape_string($this->gouv)."','".$database->escape_string($this->cp)."','".$database->escape_string($this->activite)."','".$database->escape_string($this->DRT)."')" ;

if ($database->query($requete)){
		return true;
	}else {
		return false;
	}

}

public function update_client() {

global $database;
$requete = "UPDATE pfe.clients SET rs = '".$this->rs."', rc = '".$this->rc."', mat = '".$database->escape_string($this->mat)."' , adresse = '".$database->escape_string($this->adresse)."', gouv = '".$database->escape_string($this->gouv)."', cp = '".$database->escape_string($this->cp)."', activite = '".$database->escape_string($this->activite)."', DRT='".$database->escape_string($this->DRT)."' WHERE matfis ='".$this->matfis."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_client(){
	
global $database;
$requete = "DELETE FROM pfe.clients WHERE clients.matfis ='".$this->matfis."'"	;

$database->query($requete);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

