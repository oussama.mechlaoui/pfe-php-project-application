<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;


			

}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
	<link href="../style/css/footable.bootstrap.min.css" rel="stylesheet">
	<link href="../style/css/footable.bootstrap.css" rel="stylesheet">
	<link href="../style/css/halflings.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
	
	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
			
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
				<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Accueil</a></li>
                  <li class="active" >Gestion Clients Affaires</li>
                  <li class="active">D&eacute;tails Contrats</li>
                </ol>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
               <br> <br>
                
                 <div class="col-lg-12">
						
						<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> D&eacute;tails Contrats Mobile</h3>
                            </div>
                            <div class="panel-body">
							 
								
                                <div class="table-responsive ">
								 <button type="button" id="exportButtonMobile" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                                    <table  id="mobile" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>Code Client BSCS</th>
												<th>Date Signature Contrat</th>
												<th>Dur&eacute;e</th>
                                                <th>Date Fin Contrat</th>
												<th>CA Mensuel</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offre : Nombre de lignes</th>
												<th>Charg&eacute; Client&egrave;le</th>
												<th>Client Affaires</th>
												<th>Adresse</th>
												<th>Activit&eacute;</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											   $requetemob = "SELECT * FROM pfe.contrats where type='mobile' and etat='on' and DRT='".$user->DRT."'";
			                                   $mobile = Contrats::find_all_query($requetemob);
										       $mob = new Contrats();
										foreach($mobile as $mob){
											$rqmob = "SELECT * FROM clientele WHERE id_clientele='".$mob->id_cc."'";
											$clientelemob = Clientele::find_all_query($rqmob);
											foreach ($clientelemob as $cltmob){
											$clientsmob = Client::find_clients_by_matricule($mob->matfis);

    									   echo'<tr>';
										   echo'<td>'.$mob->code.'</td>';
										   echo'<td>'.$mob->signature.'</td>';
										   echo'<td>'.$mob->duree.'</td>';
										   echo'<td>'.$mob->fin.'</td>';
										   echo'<td>'.$mob->ca_mensuel.'</td>';
										   echo'<td>'.$mob->remise.'</td>';
										   echo'<td>'.$mob->geste.'</td>';
										    echo'<td>';
											$scmob = "SELECT * FROM souscontrats where id_contrat='".$mob->id_contrat."' and matfis='".$mob->matfis."' and type='mobile'";
											$scmobile = Souscontrats::find_all_query($scmob);
											$souscont = new Souscontrats();
											foreach($scmobile as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
											echo'</td>';										
										   echo'<td>'.$cltmob->nom.' '.$cltmob->prenom.' '.$cltmob->matricule.'</td>';
										   echo'<td>'.$clientsmob->rs.' '.$clientsmob->rc.' '.$clientsmob->matfis.'</td>';
										   echo'<td>'.$clientsmob->adresse.'</td>';
										   echo'<td>'.$clientsmob->activite.'</td>';

										   
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
									    
										}
											?>
                                        </tbody>
                                    </table>
									<!-- JSPDF -->
									<div id="pdfmobile" style="display:none">
                                     <table  id="exportTableMobile" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>BSCS</th>
												<th>Client</th>
												<th>DSC</th>
												<th>Duree</th>
                                                <th>DFC</th>
												<th>CA</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offres</th>
												<th>CC</th>
												<th>Adresse</th>
												<th>Activite</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											   $requetemob = "SELECT * FROM pfe.contrats where type='mobile' and etat='on' and DRT='".$user->DRT."'";
			                                   $mobile = Contrats::find_all_query($requetemob);
										       $mob = new Contrats();
										foreach($mobile as $mob){
											$rqmob = "SELECT * FROM clientele WHERE id_clientele='".$mob->id_cc."'";
											$clientelemob = Clientele::find_all_query($rqmob);
											foreach ($clientelemob as $cltmob){
											$clientsmob = Client::find_clients_by_matricule($mob->matfis);

    									   echo'<tr>';
										   echo'<td>'.$mob->code.' </td>';
                                           echo'<td>'.$clientsmob->rs.' '.$clientsmob->rc.' '.$clientsmob->matfis.'</td>';
										   echo'<td>'.$mob->signature.'</td>';
										   echo'<td>'.$mob->duree.'</td>';
										   echo'<td>'.$mob->fin.'</td>';
										   echo'<td>'.$mob->ca_mensuel.'</td>';
										   echo'<td>'.$mob->remise.'</td>';
										   echo'<td>'.$mob->geste.'</td>';
										    echo'<td>';
											$scmob = "SELECT * FROM souscontrats where id_contrat='".$mob->id_contrat."' and matfis='".$mob->matfis."' and type='mobile'";
											$scmobile = Souscontrats::find_all_query($scmob);
											$souscont = new Souscontrats();
											foreach($scmobile as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
											echo'</td>';
										   echo'<td>'.$cltmob->nom.' '.$cltmob->prenom.' '.$cltmob->matricule.'</td>';
										   echo'<td>'.$clientsmob->adresse.'</td>';
										   echo'<td>'.$clientsmob->activite.'</td>';

										   
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
									    
										}
											?>
                                        </tbody>
                                    </table>
                                    </div>									 
									<!-- Finish -->
                                </div>
								</div>
								</div>
								
								
								<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> D&eacute;tails Contrats Fixe</h3>
                            </div>
                            <div class="panel-body">
								
                                <div class="table-responsive" >
								 <button type="button" id="exportButtonFixe" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                                    <table  id="fixe" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>Code Client BSCS</th>
												<th>Date Signature Contrat</th>
												<th>Dur&eacute;e</th>
                                                <th>Date Fin Contrat</th>
												<th>CA Mensuel</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offre : Nombre de lignes</th>
												<th>Charg&eacute; Client&egrave;le</th>
												<th>Client Affaires</th>
												<th>Adresse</th>
												<th>Activit&eacute;</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php 
											   $requetefix = "SELECT * FROM pfe.contrats where type='fixe' and etat='on' and DRT='".$user->DRT."'";
			                                   $fixe = Contrats::find_all_query($requetefix);
											   $fix = new Contrats();
										foreach($fixe as $fix){
											$rqfix = "SELECT * FROM clientele WHERE id_clientele='".$fix->id_cc."'";
											$clientelefix = Clientele::find_all_query($rqfix);
										foreach($clientelefix as $ccfix){
											$clientsfix = Client::find_clients_by_matricule($fix->matfis);
                                          

										  echo'<tr>';
										   echo'<td>'.$fix->code.'</td>';
										   echo'<td>'.$fix->signature.'</td>';
										   echo'<td>'.$fix->duree.'</td>';
										   echo'<td>'.$fix->fin.'</td>';
										   echo'<td>'.$fix->ca_mensuel.'</td>';
										   echo'<td>'.$fix->remise.'</td>';
										   echo'<td>'.$fix->geste.'</td>';
										    echo'<td>';
											$scfix = "SELECT * FROM souscontrats where id_contrat='".$fix->id_contrat."' and matfis='".$fix->matfis."' and type='fixe'";
											$scfixe = Souscontrats::find_all_query($scfix);
											$souscont = new Souscontrats();
											foreach($scfixe as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
											echo'</td>';
											
										   echo'<td>'.$ccfix->nom.' '.$ccfix->prenom.' '.$ccfix->matricule.'</td>';
										   echo'<td>'.$clientsfix->rs.' '.$clientsfix->rc.' '.$clientsfix->matfis.'</td>';
										   echo'<td>'.$clientsfix->adresse.'</td>';
										   echo'<td>'.$clientsfix->activite.'</td>';
										   
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
									<!-- JSPDF -->
									<div id="pdffixe" style="display:none">
                                    <table  id="exportTable" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>BSCS</th>
												<th>Client</th>
												<th>DSC</th>
												<th>Duree</th>
                                                <th>DFC</th>
												<th>CA</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offres</th>
												<th>CC</th>
												<th>Adresse</th>
												<th>Activite</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php 
											   $requetefix = "SELECT * FROM pfe.contrats where type='fixe' and etat='on' and DRT='".$user->DRT."'";
			                                   $fixe = Contrats::find_all_query($requetefix);
											   $fix = new Contrats();
										foreach($fixe as $fix){
											$rqfix = "SELECT * FROM clientele WHERE id_clientele='".$fix->id_cc."'";
											$clientelefix = Clientele::find_all_query($rqfix);
										foreach($clientelefix as $ccfix){
											$clientsfix = Client::find_clients_by_matricule($fix->matfis);
                                          

										  echo'<tr>';
										   echo'<td>'.$fix->code.'</td>';
										   echo'<td>'.$clientsfix->rs.' '.$clientsfix->rc.'</td>';
										   echo'<td>'.$fix->signature.'</td>';
										   echo'<td>'.$fix->duree.'</td>';
										   echo'<td>'.$fix->fin.'</td>';
										   echo'<td>'.$fix->ca_mensuel.'</td>';
										   echo'<td>'.$fix->remise.'</td>';
										   echo'<td>'.$fix->geste.'</td>';
										    echo'<td>';
											$scfix = "SELECT * FROM souscontrats where id_contrat='".$fix->id_contrat."' and matfis='".$fix->matfis."' and type='fixe'";
											$scfixe = Souscontrats::find_all_query($scfix);
											$souscont = new Souscontrats();
											foreach($scfixe as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
										   echo'</td>';											
										   echo'<td>'.$ccfix->nom.' '.$ccfix->prenom.' '.$ccfix->matricule.'</td>';
										   echo'<td>'.$clientsfix->adresse.'</td>';
										   echo'<td>'.$clientsfix->activite.'</td>';
										   
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
                                    </div>									 
									<!-- Finish -->
                                </div>
								</div>
								</div>
								
								<div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> D&eacute;tails Contrats Data</h3>
                            </div>
                            <div class="panel-body">
							
                                <div class="table-responsive">
								 <button type="button" id="exportButtonData" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                                    <table  id="data" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap" >
                                        <thead>
                                            <tr>
                                                
                                                <th >Compte de Facturation FTD</th>
												<th>Date Signature Contrat</th>
												<th>Dur&eacute;e</th>
                                                <th>Date Fin Contrat</th>
												<th>CA Mensuel</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offre : Nombre de lignes</th>
												<th>Charg&eacute; Client&egrave;le</th>
												<th>Client Affaires</th>
												<th>Adresse</th>
												<th>Activit&eacute;</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											   $requetedata = "SELECT * FROM pfe.contrats where type='data' and etat='on' and DRT='".$user->DRT."'";
			                                   $data = Contrats::find_all_query($requetedata);
										       $dat = new Contrats();
										foreach($data as $dat){
											$rqdata = "SELECT * FROM clientele WHERE id_clientele='".$dat->id_cc."'";
											$clienteledata = Clientele::find_all_query($rqdata);
							                foreach ($clienteledata as $cltdata){
											$clientsdata = Client::find_clients_by_matricule($dat->matfis);

											  echo'<tr>';
										   echo'<td>'.$dat->code.'</td>';
										   echo'<td>'.$dat->signature.'</td>';
										   echo'<td>'.$dat->duree.'</td>';
										   echo'<td>'.$dat->fin.'</td>';
										   echo'<td>'.$dat->ca_mensuel.'</td>';
										   echo'<td>'.$dat->remise.'</td>';
										   echo'<td>'.$dat->geste.'</td>';
										    echo'<td>';
											$scdat = "SELECT * FROM souscontrats where id_contrat='".$dat->id_contrat."' and matfis='".$dat->matfis."' and type='data'";
											$scdata = Souscontrats::find_all_query($scdat);
											$souscont = new Souscontrats();
											foreach($scdata as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
											echo'</td>';
											
										   echo'<td>'.$cltdata->nom.' '.$cltdata->prenom.' '.$cltdata->matricule.'</td>';
										   echo'<td>'.$clientsdata->rs.' '.$clientsdata->rc.' '.$clientsdata->matfis.'</td>';
										   echo'<td>'.$clientsdata->adresse.'</td>';
										   echo'<td>'.$clientsdata->activite.'</td>';
									
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
									<!-- JSPDF -->
									<div id="pdfdata" style="display:none">
                                       <table  id="exportTableData" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap" >
                                        <thead>
                                            <tr>
                                                
                                                <th>FTD</th>
												<th>Client</th>
												<th>DSC</th>
												<th>Duree</th>
                                                <th>DFC</th>
												<th>CA</th>
												<th>Remise</th>
												<th>Geste</th>
												<th>Offres</th>
												<th>CC</th>
												<th>Adresse</th>
												<th>Activite</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											   $requetedata = "SELECT * FROM pfe.contrats where type='data' and etat='on' and DRT='".$user->DRT."'";
			                                   $data = Contrats::find_all_query($requetedata);
										       $dat = new Contrats();
										foreach($data as $dat){
											$rqdata = "SELECT * FROM clientele WHERE id_clientele='".$dat->id_cc."'";
											$clienteledata = Clientele::find_all_query($rqdata);
							                foreach ($clienteledata as $cltdata){
											$clientsdata = Client::find_clients_by_matricule($dat->matfis);

											  echo'<tr>';
										   echo'<td>'.$dat->code.'</td>';
										   echo'<td>'.$clientsdata->rs.' '.$clientsdata->rc.' '.$clientsdata->matfis.'</td>';
										   echo'<td>'.$dat->signature.'</td>';
										   echo'<td>'.$dat->duree.'</td>';
										   echo'<td>'.$dat->fin.'</td>';
										   echo'<td>'.$dat->ca_mensuel.'</td>';
										   echo'<td>'.$dat->remise.'</td>';
										   echo'<td>'.$dat->geste.'</td>';
										    echo'<td>';
											$scdat = "SELECT * FROM souscontrats where id_contrat='".$dat->id_contrat."' and matfis='".$dat->matfis."' and type='data'";
											$scdata = Souscontrats::find_all_query($scdat);
											$souscont = new Souscontrats();
											foreach($scdata as $souscont){
											echo $souscont->offre.' : '.$souscont->nbre_lignes.' || ';
											}
											echo'</td>';
											
										   echo'<td>'.$cltdata->nom.' '.$cltdata->prenom.' '.$cltdata->matricule.'</td>';										   
										   echo'<td>'.$clientsdata->adresse.'</td>';
										   echo'<td>'.$clientsdata->activite.'</td>';
									
                                           echo'<td class="center">
									       <a class="btn btn-info" href="modcontrat.php">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppcontrat.php">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
                                    </div>									 
									<!-- Finish -->
                                </div>
							
								
								
                            </div>
                        </div>
                    </div>
                </div>				
				

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

 <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	<script src='../style/js/jquery.dataTables.min.js'></script>
	<script type="text/javascript" src='../style/js/footable.js'></script>
	<script type="text/javascript" src='../style/js/footable.min.js'></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.cont.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>

<script>
$(document).ready(function(){
    $('#mobile').dataTable();
	document.getElementById("mobile").style.width = "100%";


	
});
</script>

<script>
$(document).ready(function(){
    $('#fixe').dataTable();
	document.getElementById("fixe").style.width = "100%";


	
});
</script>

<script>
$(document).ready(function(){
    $('#data').dataTable();
	document.getElementById("data").style.width = "100%";


	
});
</script>

	<script>
$("#exportButtonMobile").hover(
function () {
  
   $(this).text("PDF");
});
</script>

	<script>
$("#exportButtonFixe").hover(
function () {
  
   $(this).text("PDF");
});
</script>

	<script>
$("#exportButtonData").hover(
function () {
  
   $(this).text("PDF");
});
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButtonMobile").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#exportTableMobile",
                schema: {
                    type: "table",
                    fields: {
                        BSCS: { type : String },
						Client: { type : String },
                        DSC: { type: String },
						DFC: { type: String },
						CA: { type: String },
						Remise: {type: String},
						Geste: {type: String},
						Offres: {type: String}
						
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "BSCS", title: "Code BSCS", width: 100 },
						{ field: "Client", title: "Client Affaires", width: 100 },
                        { field: "DSC", title: "Date Signature", width: 100 },
						{ field: "DFC", title: "Date Fin Contrat", width: 100 }, 
						{ field: "CA", title: "CA Mensuel", width: 80 },
						{ field: "Remise", title: "Remise", width: 60 },
					    { field: "Geste", title: "Geste", width: 80 },
						{ field: "Offres", title: "Offres", width: 140 },
					    

                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "DetailsContratsMobile"
                });
            });
        });
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButtonFixe").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#exportTable",
                schema: {
                    type: "table",
                    fields: {
                        BSCS: { type : String },
						Client: { type : String },
                        DSC: { type: String },
						DFC: { type: String },
						CA: { type: String },
						Remise: {type: String},
						Geste: {type: String},
						Offres: {type: String}, 
						
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "BSCS", title: "Code BSCS", width: 100 },
						{ field: "Client", title: "Client Affaires", width: 100 },
                        { field: "DSC", title: "Date Signature ", width: 100 },
						{ field: "DFC", title: "Date Fin Contrat", width: 100 }, 
					    { field: "CA", title: "CA Mensuel", width: 80 },
						{ field: "Remise", title: "Remise", width: 60 },
					    { field: "Geste", title: "Geste", width: 80 },
					    { field: "Offres", title: "Offres", width: 140 }, 
					    

                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "DetailsContratsFixe"
                });
            });
        });
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButtonData").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#exportTableData",
                schema: {
                    type: "table",
                    fields: {
                        FTD: { type : String },
						Client: { type : String },
                        DSC: { type: String },
						DFC: { type: String },
						CA: { type: String },
						Remise: {type: String},
						Geste: {type: String},
						Offres: {type: String}, 
						
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "FTD", title: "Code FTD", width: 100 },
						{ field: "Client", title: "Client Affaires", width: 100 },
                        { field: "DSC", title: "Date Signature ", width: 100 },
						{ field: "DFC", title: "Date Fin Contrat", width: 100 }, 
					    { field: "CA", title: "CA Mensuel", width: 80 },
						{ field: "Remise", title: "Remise", width: 60 },
					    { field: "Geste", title: "Geste", width: 80 },
					    { field: "Offres", title: "Offres", width: 140 }, 
					    

                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "DetailsContratsData"
                });
            });
        });
    });
</script>





</body>

</html>

