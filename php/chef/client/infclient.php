<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;


			

}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
	<link href="../style/css/footable.bootstrap.min.css" rel="stylesheet">
	<link href="../style/css/footable.bootstrap.css" rel="stylesheet">
	<link href="../style/css/halflings.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
			
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
				<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Accueil</a></li>
                  <li class="active" >Gestion Clients Affaires</li>
                  <li class="active">Consultation Client Affaires</li>
                </ol>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <br> <br>
                
                 <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> Consultation Client Affaires</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive ">
								<button type="button" id="exportButton" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
                                    <table  id="myTable" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th >Charg&eacute; Client&egrave;le</th>
												<th >Raison Sociale</th>
												<th >RC</th>
                                                <th >Matricule Fiscale</th>
												<th >Adresse</th>
												<th>Gouvernorat</th>
												<th>Activit&eacute; Client</th>
												<th>Code Postal Client</th>
												<th >DRT</th>
                                                <th>Nom Interlocuteur</th>
												<th>Pr&eacute;nom Interlocuteur</th>
												<th>Pi&egrave;ce d'identit&eacute;</th>
												<th>Identit&eacute;</th>
                                                <th>Poste</th>
                                                <th>Fixe</th>
												<th>Mobile</th>
												<th>Email</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
								
											   $requete = "SELECT * FROM clients , clientele  where clients.mat = clientele.matricule and clients.DRT = '".$user->DRT."' and clientele.DRT = '".$user->DRT."' ";
			                                   $RS = $database->query($requete);
											   while ($row = mysqli_fetch_array($RS)) {
											$rqq = "SELECT * FROM pfe.interlocuteur WHERE matfis='".$row['matfis']."'";
											$inter = Interlocuteur::find_all_query($rqq);
										foreach ($inter as $int){
                                          

    									   echo'<tr>';
										   echo'<td>'.$row['matricule'].' '.$row['nom'].' '.$row['prenom'].'</td>';
										   echo'<td>'.$row['rs'].'</td>';
										   echo'<td>'.$row['rc'].'</td>';
										   echo'<td>'.$row['matfis'].'</td>';
										   echo'<td>'.$row['adresse'].'</td>';
										   echo'<td>'.$row['gouv'].'</td>';
										   echo'<td>'.$row['activite'].'</td>';
										   echo'<td>'.$row['cp'].'</td>';
										   echo'<td>'.$row['DRT'].'</td>';
										   echo'<td>'.$int->nom.'</td>';
										   echo'<td>'.$int->prenom.'</td>';
										   echo'<td>'.$int->piece.'</td>';
										   echo'<td>'.$int->identite.'</td>';
										   echo'<td>'.$int->poste.'</td>';
										   echo'<td>'.$int->fixe.'</td>';
										   echo'<td>'.$int->mobile.'</td>';
										   echo'<td>'.$int->email.'</td>';
										   
										   
                                           echo'<td class="center">
										<a href="listactions.php?matis='.$row['matfis'].'&amp;mat='.$row['matricule'].'&amp;cin_int='.$int->identite.'" class="btn btn-success"><i class="halflings-icon white tasks"></i>  Actions</a>
									       <a class="btn btn-info" href="modclient.php?id_clientele='.$row['id_clientele'].'&amp;nom='.$row['nom'].'&amp;prenom='
										                          .$row['prenom'].'&amp;mat='.$row['matricule'].'&amp;rs='.$row['rs'].'&amp;rc='.$row['rc'].'
																   &amp;matfis='.$row['matfis'].'&amp;adresse='.$row['adresse'].'&amp;gouv='.$row['gouv'].'&amp;cp='.$row['cp'].'
										                           &amp;activite='.$row['activite'].'&amp;id_int='.$int->id_int.'&amp;nomint='.$int->nom.'&amp;prenomint='.$int->prenom.'&amp;identite='.$int->identite.'
																   &amp;poste='.$int->poste.'&amp;fixe='.$int->fixe.'&amp;mobile='.$int->mobile.'&amp;email='.$int->email.'">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppclient.php?matfis='.$row['matfis'].'&amp;idint='.$int->id_int.'">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
					<!-- JSPDF -->
					<div id="pdf" style="display:none">
					<table  id="exportTable" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th >CC</th>
												<th >Client</th>
												<th >Adresse</th>
												<th>Interlocuteur</th>
												<th>Activite</th>
												<th>CP</th>
												<th >DRT</th>
                                                <th>Poste</th>
                                                <th>Fixe</th>
												<th>Mobile</th>
												<th>Email</th>
												<th>Actions</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
								
											   $requete = "SELECT * FROM clients , clientele  where clients.mat = clientele.matricule and clients.DRT = '".$user->DRT."' and clientele.DRT = '".$user->DRT."' ";
			                                   $RS = $database->query($requete);
											   while ($row = mysqli_fetch_array($RS)) {
											$rqq = "SELECT * FROM pfe.interlocuteur WHERE matfis='".$row['matfis']."'";
											$inter = Interlocuteur::find_all_query($rqq);
										foreach ($inter as $int){
                                          

    									   echo'<tr>';
										   echo'<td>'.$row['matricule'].' '.$row['nom'].' '.$row['prenom'].'</td>';
										   echo'<td>'.$row['matfis'].' '.$row['rs'].' '.$row['rc'].'</td>';
										   echo'<td>'.$row['adresse'].', '.$row['gouv'].'</td>';
										   echo'<td>'.$int->nom.' '.$int->prenom.' '.$int->piece.': '.$int->identite.'</td>';
										   echo'<td>'.$row['activite'].'</td>';
										   echo'<td>'.$row['cp'].'</td>';										   
										   echo'<td>'.$row['DRT'].'</td>';
										   echo'<td>'.$int->poste.'</td>';
										   echo'<td>'.$int->fixe.'</td>';
										   echo'<td>'.$int->mobile.'</td>';
										   echo'<td>'.$int->email.'</td>';
										   
										   
                                           echo'<td class="center">
										<a href="listactions.php?matis='.$row['matfis'].'&amp;mat='.$row['matricule'].'&amp;cin_int='.$int->identite.'" class="btn btn-success"><i class="halflings-icon white tasks"></i>  Actions</a>
									       <a class="btn btn-info" href="modclient.php?id_clientele='.$row['id_clientele'].'&amp;nom='.$row['nom'].'&amp;prenom='
										                          .$row['prenom'].'&amp;mat='.$row['matricule'].'&amp;rs='.$row['rs'].'&amp;rc='.$row['rc'].'
																   &amp;matfis='.$row['matfis'].'&amp;adresse='.$row['adresse'].'&amp;gouv='.$row['gouv'].'&amp;cp='.$row['cp'].'
										                           &amp;activite='.$row['activite'].'&amp;id_int='.$int->id_int.'&amp;nomint='.$int->nom.'&amp;prenomint='.$int->prenom.'&amp;identite='.$int->identite.'
																   &amp;poste='.$int->poste.'&amp;fixe='.$int->fixe.'&amp;mobile='.$int->mobile.'&amp;email='.$int->email.'">
										      <i class="halflings-icon white edit"></i>                                            
									       </a>
									       <a class="btn btn-danger" href="suppclient.php?matfis='.$row['matfis'].'&amp;idint='.$int->id_int.'">
										     <i class="halflings-icon white trash"></i> 
									       </a>
								           </td>';
										   echo'</tr>';
										}
										}
											?>
                                        </tbody>
                                    </table>
					</div>
					<!-- Finish -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>				
				

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	<script src='../style/js/jquery.dataTables.min.js'></script>
	<script type="text/javascript" src='../style/js/footable.js'></script>
	<script type="text/javascript" src='../style/js/footable.min.js'></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>
	
	
	
<script>
$(document).ready(function(){
    $('#myTable').dataTable();
	document.getElementById('myTable').style.width = "100%";


	
});

</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButton").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#exportTable",
                schema: {
                    type: "table",
                    fields: {
                        CC: { type : String },
                        Client: { type: String },
						Adresse: { type: String },
						Interlocuteur: {type: String},
						Activite: {type: String},
						DRT: {type: String}
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "CC", title: "Charge Clientele", width: 100 },
                        { field: "Client", title: "Client Affaires", width: 100 },
						{ field: "Adresse", title: "Adresse", width: 100 }, 
						{ field: "Interlocuteur", title: "Interlocuteur", width: 140 },
						{ field: "Activite", title: "Activite", width: 100 },
						{ field: "DRT", title: "DRT", width: 100 }

                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "InformationsClients"
                });
            });
        });
    });
</script>

	<script>
$("#exportButton").hover(
function () {
  
   $(this).text("PDF");
});
</script>


</body>

</html>

