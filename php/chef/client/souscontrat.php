<?php require_once("init.php"); ?>

<?php

Class Souscontrats {
	
	public $id_sc;
	public $id_contrat;
	public $code_client;
	public $matfis;
	public $type;
	public $nbre_lignes;
	public $offre;
	

public static function find_souscontrat_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.souscontrats where id_sc="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_souscontrat(){

global $database;
$requete = "INSERT INTO pfe.souscontrats (id_contrat , code_client , matfis , type , nbre_lignes , offre) VALUES ('".$database->escape_string($this->id_contrat)."' ,'".$database->escape_string($this->code_client)."', '".$database->escape_string($this->matfis)."' , '".$database->escape_string($this->type)."' , '".$database->escape_string($this->nbre_lignes)."' , '".$database->escape_string($this->offre)."')" ;

if ($database->query($requete)){
	    $this->id_sc = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_souscontrat() {

global $database;
$requete = "UPDATE pfe.souscontrats SET code_client= '".$database->escape_string($this->code_client)."', type='".$database->escape_string($this->id_sc)."' , nbre_lignes='".$database->escape_string($this->nbre_lignes)."' , offre = '".$this->offre."' WHERE id_sc ='".$this->id_sc."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_souscontrat(){
	
global $database;
$requete = "DELETE FROM pfe.souscontrats WHERE mobile.code ='".$this->sc."'"	;
$sql= "ALTER TABLE pfe.souscontrats AUTO_INCREMENT = ".$this->id_sc.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

