<?php require_once("init.php"); ?>

<?php

Class Clientele {
	
	public $id_clientele;
	public $nom;
	public $prenom;
	public $matricule;
	public $DRT;

	

public static function find_clientele_by_matricule($mat){
global $database;
$requete = "SELECT * FROM pfe.clientele where matricule="."'".$mat."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_clientele(){

global $database;
$requete = "INSERT INTO pfe.clientele (nom, prenom ,matricule,DRT) VALUES ('".$database->escape_string($this->nom)."', '".$database->escape_string($this->prenom)."' ,'".$database->escape_string($this->matricule)."','".$database->escape_string($this->DRT)."')" ;

if ($database->query($requete)){
	    $this->id_clientele = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_clientele() {

global $database;
$requete = "UPDATE pfe.clientele SET nom = '".$this->nom."', prenom = '".$this->prenom."', matricule ='".$this->matricule."' , DRT = '".$database->escape_string($this->DRT)."' WHERE id_clientele ='".$this->id_clientele."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_clientele(){
	
global $database;
$requete = "DELETE FROM pfe.clientele WHERE clientele.id_clientele ='".$this->id_clientele."'"	;
$sql= "ALTER TABLE pfe.clientele AUTO_INCREMENT = ".$this->id_clientele.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

