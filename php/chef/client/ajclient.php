<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;
			
			if (isset($_POST['ajouter'])){
			   if( !empty($_POST['rs']) && !empty($_POST['rc'])
					&& !empty($_POST['matfis']) && !empty($_POST['adresse']) && !empty($_POST['gouv']) && !empty($_POST['cp']) && !empty($_POST['activ'])
					&& !empty($_POST['nom1']) && !empty($_POST['prenom1']) && !empty($_POST['identite']) && !empty($_POST['poste']) 
					&& !empty($_POST['fixe']) && !empty($_POST['mobile']) && !empty($_POST['email'])  )
					{
				 if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['mat']))		
				 {     $clientele = new Clientele();
					    $clientele->nom = $_POST['nom'];
					    $clientele->prenom = $_POST['prenom'];
					    $clientele->matricule = $_POST['mat'];
					    $clientele->DRT = $_POST['DRT'];
					    $clientele->add_clientele();
				 
				 $client = new Client();
						$client->rs = $_POST['rs'];
						$client->rc = $_POST['rc'];
						$client->matfis = $_POST['matfis'];
						$client->mat = $_POST['mat'];
						$client->adresse = $_POST['adresse'];
						$client->gouv = $_POST['gouv'];
						$client->cp = $_POST['cp'];
						$client->activite = $_POST['activ'];
						$client->DRT = $user->DRT;
						$client->add_client();
						
						$interlocuteur = new Interlocuteur();
						$interlocuteur->nom = $_POST['nom1'];
						$interlocuteur->prenom = $_POST['prenom1'];
						$interlocuteur->piece = $_POST['piece'];
						$interlocuteur->identite = $_POST['identite'];
						$interlocuteur->poste = $_POST['poste'];
						$interlocuteur->fixe = $_POST['fixe'];
						$interlocuteur->mobile = $_POST['mobile'];
						$interlocuteur->email = $_POST['email'];
						$interlocuteur->matfis = $_POST['matfis'];
						$interlocuteur->add_interlocuteur();				 
				 
				 echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				 
				 }
				 
				 else  {
					 
					  $charge = Clientele::find_clientele_by_matricule($_POST['choice']);
				 
				       $client = new Client();
						$client->rs = $_POST['rs'];
						$client->rc = $_POST['rc'];
						$client->matfis = $_POST['matfis'];
						$client->mat = $_POST['choice'];
						$client->adresse = $_POST['adresse'];
						$client->gouv = $_POST['gouv'];
						$client->cp = $_POST['cp'];
						$client->activite = $_POST['activ'];
						$client->DRT = $user->DRT;
						$client->add_client();
						
						$interlocuteur = new Interlocuteur();
						$interlocuteur->nom = $_POST['nom1'];
						$interlocuteur->prenom = $_POST['prenom1'];
						$interlocuteur->piece = $_POST['piece'];
						$interlocuteur->identite = $_POST['identite'];
						$interlocuteur->poste = $_POST['poste'];
						$interlocuteur->fixe = $_POST['fixe'];
						$interlocuteur->mobile = $_POST['mobile'];
						$interlocuteur->email = $_POST['email'];
						$interlocuteur->matfis = $_POST['matfis'];
						$interlocuteur->add_interlocuteur();
				 
				 }
								
					} else {
					echo '<script language="Javascript">
                <!--
                alert("Veuillez remplir les champs vides");
				
                // -->
                </script>';
				}
				echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				
				echo '<script language="Javascript">
                <!--
                document.location.replace("infclient.php");
                // -->
                </script>';
						}

			

}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
		

	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Accueil</a></li>
                  <li class="active" >Gestion Clients Affaires</li>
                  <li class="active">Ajout Client Affaires</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
               <br> <br>
				 <div class="row">
					
					<form name="ajvente"  method=POST>	
                    <div class="col-lg-12">
                        <div class="panel panel-default">					
                            <div class="panel-heading">		                
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Charg&eacute; Client&egrave;le</h3>
								</div> 
                             <div class="panel-body">
                                      <div id="div2" style="display:block">
							            <div class="col-xs-4">
                                                <select class="form-control" name="choice" id="mySelect">
												            <option value="" selected>Choisir un charg&eacute; client&egrave;le existant</option>
															<?php
                                                            $sel = "select * from clientele where DRT='".$user->DRT."'";
                                                            $cltl = Clientele::find_all_query($sel);
															$cltle = new Clientele();
															foreach($cltl as $cltle){
																echo'<option value="'.$cltle->matricule.'">'.$cltle->nom.' '.$cltle->prenom.' </option>';
															}
															
															?>
                                                 </select>
										</div>
										</div>
									 <button type="button" id="dis" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
											
                                  
                                  <div id="div1" style="display:none">
							 
					                    <div class="col-xs-3">
                                              <label id="att" for="exampleSelect1" >Nom </label>
					                                <input class="form-control" type="text" id="noml" name="nom"  >
								        </div>										
					                    <div class="col-xs-3">
                                              <label id="att" for="exampleSelect1" >Pr&eacute;nom </label>
					                                <input class="form-control" type="text" id="prenoml" name="prenom"  >
													
								        </div>				              
					                    <div class="col-xs-3">
										<label id="att" for="exampleSelect1" >Matricule </label>
					                                <input class="form-control" type="text" name="mat" id="matl" >
								        </div>
										
										<div class="col-xs-3">
                                        <label for="exampleSelect1">DRT</label>
                                        <select class="form-control" name="DRT" id="exampleSelect1">
								        <?php $region = array("1" => "Ariana","2" => "B&egrave;ja","3" => "Ben Arous","4" => "Bizerte","5" => "Gabes","6" => "Gafsa","7" => "Jendouba","8" => "Kairouan","9" => "Kasserine","10" => "Kebili","11" => "Le Kef","12" => "Mahdia","13" => "La Mannouba","14" => "Madanine","15" => "Monastir","16" => "Nabeul","17" => "Sfax","18" => "Sidi Bouzid","19" => "Siliana","20" => "Sousse","21" => "Tataouine","22" => "Tozeur","23" => "Tunis","24" => "Zaghouan");
                                        for ($i=1 ; $i <25 ; $i++) {                                          
									    echo'<option value="'.$region[$i].'">'.$region[$i].' </option>';
									    }
                                       
                                        ?>									   
                                        </select>
                                        </div>
										
										<div class="col-xs-3">
                                        <fieldset class="form-group">
										<br>
										<button type="button" id="diss" class="btn btn-warning btn-circle"><i class="glyphicon glyphicon-remove"></i></button>
                                        </fieldset>
										</div>
										
                                  </div>	

								  
					         </div>                           
							  <div class="panel-heading">							                
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>D&eacute;tails Client</h3>
								</div> 
								
                             <div class="panel-body">				                
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Raison Sociale </label>
					                                <input class="form-control" type="text" name="rs" >
								        </div>					         
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">RC / Patente </label>
					                                <input class="form-control" type="text" name="rc" >
								        </div>
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Matricule Fiscale </label>
					                                <input class="form-control" type="text" name="matfis" >
								        </div>
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Adresse </label>
					                                <input class="form-control" type="text" name="adresse" >
								        </div>
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Gouvernorat </label>
					                                <input class="form-control" type="text" name="gouv" >
								        </div>												   
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Code Postal </label>
					                                <input class="form-control" type="text" name="cp" >
								        </div>					            								   								  
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Activit&eacute; </label>
											  <select class="form-control" name="activ" >
												            <option value="">Veuillez choisir une activit&eacute; </option>
												            <option value="Agroalimentaire">Agroalimentaire </option>
															<option value="Chimie, Plastique, Santé">Chimie, Plastique, Santé </option>
															<option value="Construction, Bâtiment, Bois, Habitat">Construction, Bâtiment, Bois, Habitat</option>
															<option value="Energie, Environnement">Energie, Environnement</option>
															<option value="Enseignement, formation - Administrations">Enseignement, formation - Administrations </option>
															<option value="Informatique, Internet, R&D">Informatique, Internet, R&D </option>
															<option value="Loisirs, Tourisme, Culture">Loisirs, Tourisme, Culture </option>
															<option value="Matériel électrique, électronique, optique">Matériel électrique, électronique, optique </option>
															<option value="Métallurgie, mécanique et sous-traitance">Métallurgie, mécanique et sous-traitance </option>
															<option value="Négoce, grande distribution, détaillants">Négoce, grande distribution, détaillants </option>
															<option value="Papier, impression, édition">Papier, impression, édition </option>
															<option value="Produits minéraux">Produits minéraux </option>
															<option value="Services aux entreprises">Services aux entreprises </option>
															<option value="Textile, Habillement, Cuir, Horlogerie, Bijouterie">Textile, Habillement, Cuir, Horlogerie, Bijouterie </option>
															<option value="Transports et logistique">Transports et logistique </option>																																																											
                                                 </select> 
								        </div>
					         </div>
							 
							  <div class="panel-heading">			              
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Interlocuteur</h3>
								</div> 
                             <div class="panel-body">							    				                   
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Nom </label>
					                                <input class="form-control" type="text" name="nom1" >
								        </div>					            								  
					                    <div class="col-xs-4">
                                              <label for="exampleSelect2">prenom </label>
					                                <input class="form-control" type="text" name="prenom1" >
								        </div>					               								   
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Pi&egrave;ce d'indentit&eacute; </label>
					                                <select class="form-control" name="piece" id="exampleSelect1">
								                        <option value="cin"selected>Cin </option>
														<option value="passeport">Passeport</option>
												    </select>
								        </div>					               					               
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">N&deg; pi&egrave;ce d'indentit&eacute; </label>
					                                <input class="form-control" type="text" name="identite" >
								        </div>					           						           
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Poste </label>
					                                <input class="form-control" type="text" name="poste" >
								        </div>					                								  
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Fixe </label>
					                                <input class="form-control" type="text" name="fixe" >
								        </div>					               								
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Mobile </label>
					                                <input class="form-control" type="text" name="mobile" >
								        </div>					         								  
					                    <div class="col-xs-4">
                                              <label for="exampleSelect1">Email </label>
					                                <input class="form-control" type="email" name="email" >
								        </div>					             
					         </div>						 
                        </div> 
						 
						 <button type="submit" name="ajouter" class="btn btn-primary">Ajouter</button>
								  <button type="submit" name="annuler" class="btn btn-primary">Annulation</button>
						 
                    </div>
			
                                   
                               
					</form>

			  </div> 
             
            </div>
                                

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
		<script>	
	
	$(document).ready(function() {
    $("#dis").click(function(){
       $("#div1").show();
	    $("#div2").hide();
		$("#dis").hide();
    }); 
});
	</script>
	
	<script>	
	
	$(document).ready(function() {
    $("#diss").click(function(){
       $("#div2").show();
	    $("#div1").hide();
		$("#dis").show();
    }); 
});
	</script>
	
	<script>
$("#dis").hover(
function () {
  
   $(this).text("Nouveau Chargé Clientèle");
});
</script>

	<script>
$("#diss").hover(
function () {
  
   $(this).text("Retour");
});
</script>
	

</body>

</html>

