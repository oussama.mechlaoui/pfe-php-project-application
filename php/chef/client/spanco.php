<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			global $database;
			
		    $requetemob = "select * from forfait where offre='mobile' and etat='active'";
			$offresmob = Forfait::find_all_query($requetemob);
			
			$requetefix = "select * from forfait where offre='fixe' and etat='active'";
			$offresfix = Forfait::find_all_query($requetefix);
			
			$requetedata = "select * from forfait where offre='data' and etat='active'";
			$offresdata = Forfait::find_all_query($requetedata);
					
			if(isset($_POST['actionner'])) {
				$cl = Client::find_clients_by_matricule($_POST['client']);
				$action = new Actions();
				$action->act = $_POST['action'];
				$action->date = $_POST['date1'];
				$action->commentaire = $_POST['comm'];
				$action->matricule = $cl->mat;
				$action->matfis = $_POST['client'];
				$action->DRT = $user->DRT;
				$action->add_actions();
				
				echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				echo '<script language="Javascript">
                <!--
                document.location.replace("spanco.php");
                // -->
                </script>';
				
			}
			
			if(isset($_POST['ajoutermobile'])){
			if( !empty($_POST['bscsmob']) && !empty($_POST['clientmob'])
					&& !empty($_POST['datedebutmob']) && !empty($_POST['dureemob']) && !empty($_POST['datefinmob']) && !empty($_POST['nbremobile']) && !empty($_POST['camob'])
					&& !empty($_POST['remisemob']) && !empty($_POST['gestemob'])   )
			{
				
				$contrat = new Contrats();
				$contrat->code = $_POST['bscsmob'];
				$contrat->matfis = $_POST['clientmob'];
				$cl = Client::find_clients_by_matricule($_POST['clientmob']);
				$clientele = Clientele::find_clientele_by_matricule($cl->mat);
				$contrat->id_cc = $clientele->id_clientele;
				$contrat->signature = $_POST['datedebutmob'];
				$contrat->duree = $_POST['dureemob'];
				$contrat->fin = $_POST['datefinmob'];
				$contrat->nbre_offres = $_POST['nbremobile'];
                $contrat->type ="mobile";
				$contrat->etat="on";
				$contrat->ca_mensuel = $_POST['camob'];
				$contrat->remise = $_POST['remisemob'];
				$contrat->geste = $_POST['gestemob'];
				$contrat->DRT = $user->DRT;
				$contrat->add_contrat();
				$myreq = "select * from contrats where code='".$_POST['bscsmob']."' and type ='mobile' and matfis='".$_POST['clientmob']."'";
				$contrats = Contrats::find_all_query($myreq);

				foreach($contrats as $cont){
				for($cpt=0; $cpt < $_POST['nbremobile'] ; $cpt++){
					$str1 = "nombre".$cpt;
					$str2 = "offre".$cpt;
					$souscontrat = new Souscontrats();
					$souscontrat->id_contrat = $cont->id_contrat;
					$souscontrat->code_client = $_POST['bscsmob'];
					$souscontrat->matfis = $_POST['clientmob'];
					$souscontrat->type ="mobile";
					$souscontrat->nbre_lignes = $_POST[$str1];
					$souscontrat->offre = $_POST[$str2];					
					$souscontrat->add_souscontrat();
					}
					
				}
				echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				echo '<script language="Javascript">
                <!--
                document.location.replace("spanco.php");
                // -->
                </script>';
			} else {
			    echo '<script language="Javascript">
                <!--
                alert("Veuillez remplir les champs vides");
				
                // -->
                </script>';	
			}
				
			}
			
			if(isset($_POST['ajouterfixe'])){
			if( !empty($_POST['bscsfix']) && !empty($_POST['clientfix'])
					&& !empty($_POST['datedebutfix']) && !empty($_POST['dureefix']) && !empty($_POST['datefinfix']) && !empty($_POST['nbrefix']) && !empty($_POST['cafix'])
					&& !empty($_POST['remisefix']) && !empty($_POST['gestefix'])   )
			{
				$contrat = new Contrats();
				$contrat->code = $_POST['bscsfix'];
				$contrat->matfis = $_POST['clientfix'];
				$cl = Client::find_clients_by_matricule($_POST['clientfix']);
				$clientele = Clientele::find_clientele_by_matricule($cl->mat);
				$contrat->id_cc = $clientele->id_clientele;
				$contrat->signature = $_POST['datedebutfix'];
				$contrat->duree = $_POST['dureefix'];
				$contrat->fin = $_POST['datefinfix'];
				$contrat->nbre_offres = $_POST['nbrefix'];
                $contrat->type ="fixe";
				$contrat->etat="on";
				$contrat->ca_mensuel = $_POST['cafix'];
				$contrat->remise = $_POST['remisefix'];
				$contrat->geste = $_POST['gestefix'];
				$contrat->DRT = $user->DRT;
				$contrat->add_contrat();
				$myreq = "select * from contrats where code='".$_POST['bscsfix']."' and type ='fixe' and matfis='".$_POST['clientfix']."'";
				$contrats = Contrats::find_all_query($myreq);
				
				foreach($contrats as $cont){				
				for($cpt=0; $cpt < $_POST['nbrefix'] ; $cpt++){
					$str1 = "nombrefix".$cpt;
					$str2 = "offrefix".$cpt;
					$souscontrat = new Souscontrats();
					$souscontrat->id_contrat = $cont->id_contrat;
					$souscontrat->code_client = $_POST['bscsfix'];
					$souscontrat->matfis = $_POST['clientfix'];
					$souscontrat->type ="fixe";
					$souscontrat->nbre_lignes = $_POST[$str1];
					$souscontrat->offre = $_POST[$str2];					
					$souscontrat->add_souscontrat();
					
				}
				}
				echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				echo '<script language="Javascript">
                <!--
                document.location.replace("spanco.php");
                // -->
                </script>';
			} else {
			    echo '<script language="Javascript">
                <!--
                alert("Veuillez remplir les champs vides");
				
                // -->
                </script>';	
			}
			}
			
			if(isset($_POST['ajouterdata'])){
				if( !empty($_POST['ftddata']) && !empty($_POST['clientdata'])
					&& !empty($_POST['datedebutdata']) && !empty($_POST['dureedata']) && !empty($_POST['datefindata']) && !empty($_POST['nbredata']) && !empty($_POST['cadata'])
					&& !empty($_POST['remisedata']) && !empty($_POST['gestedata'])   )
			{
				$contrat = new Contrats();
				$contrat->code = $_POST['ftddata'];
				$contrat->matfis = $_POST['clientdata'];
				$cl = Client::find_clients_by_matricule($_POST['clientdata']);
				$clientele = Clientele::find_clientele_by_matricule($cl->mat);
				$contrat->id_cc = $clientele->id_clientele;
				$contrat->signature = $_POST['datedebutdata'];
				$contrat->duree = $_POST['dureedata'];
				$contrat->fin = $_POST['datefindata'];
				$contrat->nbre_offres = $_POST['nbredata'];
                $contrat->type ="data";
				$contrat->etat="on";
				$contrat->ca_mensuel = $_POST['cadata'];
				$contrat->remise = $_POST['remisedata'];
				$contrat->geste = $_POST['gestedata'];
				$contrat->DRT = $user->DRT;
				$contrat->add_contrat();
				$myreq = "select * from contrats where code='".$_POST['ftddata']."' and type ='data' and matfis='".$_POST['clientdata']."'";
				$contrats = Contrats::find_all_query($myreq);
				foreach($contrats as $cont){
					
				for($cpt=0; $cpt < $_POST['nbredata'] ; $cpt++){
					$str1 = "nombredata".$cpt;
					$str2 = "offredata".$cpt;
					$souscontrat = new Souscontrats();
					$souscontrat->id_contrat = $cont->id_contrat;
					$souscontrat->code_client = $_POST['ftddata'];
					$souscontrat->matfis = $_POST['clientdata'];
					$souscontrat->type ="data";
					$souscontrat->nbre_lignes = $_POST[$str1];
					$souscontrat->offre = $_POST[$str2];					
					$souscontrat->add_souscontrat();
			
			    } 
				}
				echo '<script language="Javascript">
                <!--
                alert("Ajout avec succès");
				
                // -->
                </script>';
				echo '<script language="Javascript">
                <!--
                document.location.replace("spanco.php");
                // -->
                </script>';
			}  else {
			    echo '<script language="Javascript">
                <!--
                alert("Veuillez remplir les champs vides");
				
                // -->
                </script>';	
			}
            }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT chef commercial - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
	 <link href="../style/css/agenda.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
		

	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo $user->espace.': Chef Agence' ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Accueil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Accueil</a></li>
                  <li class="active" >Gestion Clients Affaires</li>
                  <li class="active">SPANCO</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <br> <br>
                
				 <div class="row">
					
                    <div class="col-lg-12">
                        <div class="panel panel-default">					
                            <div class="panel-heading">		                
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>SPANCO & Contrats</h3>
								</div> 
                             <div class="panel-body">
							 
							 
                                      <div id="div2" style="display:block">
							             <fieldset class="form-group">
										 <div class="col-xs-6">
                                                <label id="att" for="exampleSelect1">Selectionner SPANCO</label>
                                                 <select class="form-control" name="choice" id="mySelect" onChange="val(this.value);">
												            <option selected>Selectionner votre choix </option>
												            <option value="a">Effectuer une action</option>
															<option value="c">Consulter Calendrier</option>
															<option value="b">G&eacuten&eacuterer un contrat</option>
															
                                                 </select>
										 </div>		 
										</fieldset>
										
										</div>
										</div>
										
										
										<div id="spancohead" class="panel-heading" style="display:none">			              
                                           <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>SPANCO</h3>
								           </div> 
										<div id="spanco" style="display:none">
                                        <div class="panel-body">
										 <form name="ajaction" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
										     <fieldset class="form-group">
										     <div class="col-xs-6">
											     <label id="att"  name="clt" for="exampleSelect1">Clients</label>
                                                 <select class="form-control" name="client" id="mySelect1">
												     <?php
													 $requete = "select * from clients";
													 $client = Client::find_all_query($requete);
													 $clt = new Client();
													 foreach($client as $clt){
						                        echo' <option value="'.$clt->matfis.'">'.$clt->rs.' '.$clt->rc.' '.$clt->activite.'</option>';
													 }
													?>
															
                                                 </select>
											 </div>	
										     </fieldset>
											 
											 <fieldset class="form-group">
										 <div class="col-xs-6">
                                                <label id="att" for="exampleSelect1">Actions</label>
                                                 <select class="form-control" name="action" id="mySelect2">
												            <option value="Suspect">Suspect</option>
															<option value="Prospect">Prospect</option>
															<option value="Approche">Approche</option>
															<option value="Negociation">Negociation</option>
															<option value="Commande">Commande</option>
															
                                                 </select>
										 </div>	
										 </fieldset>
										 
										 <fieldset class="form-group">
										 <div class="col-xs-6">
										 <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="date1" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                         </div>
                                         </div>
										 </div>	
										 </fieldset>
										 
										 <fieldset class="form-group">
										 <div class="col-xs-6">
										 <label id="att" for="exampleSelect1">Commentaire</label>
                                                 <select class="form-control" name="comm" id="mySelect3" onChange="val(this.value);">
												            <option value="pas de reponse">Pas de r&eacute;ponse</option>
															<option value="interesse">Int&eacute;ress&eacute </option>
															<option value="non interesse">Non int&eacute;ress&eacute </option>
                                                 </select>
										 </div>	
										 </fieldset>
										    <button type="submit" name="actionner" class="btn btn-primary">Actionner</button>
										</form>
										</div>
										</div>
										
										 <div id="calendarhead" class="panel-heading" style="display:none">			              
                                           <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Calendrier des actions</h3>
								           </div>
										 <div id="calendar" style="display:none">
										   <div class="panel-body">
        <div class="agenda">
        <div class="table-responsive">
		<button type="button" id="exportButton" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button>
            <table id="action" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Commentaire</th>
						<th>CC</th>
						<th>Interlocuteur</th>
						<th>Client</th>
				
                    </tr>
                </thead>
                <tbody>
                    <!-- Single event in a single day -->
					<?php
					  $requete = "SELECT * FROM actions where DRT='".$user->DRT."' ORDER BY date DESC";
					  $action = Actions::find_all_query($requete);
					  foreach($action as $act) {
						   $rq = "SELECT * FROM interlocuteur WHERE matfis ='".$act->matfis."'";
						   $int = Interlocuteur::find_all_query($rq);
						   foreach($int as $inter){
						   $mons = array("Jan" => "Janvier", "Feb" => "Fevrier", "Mar" => "Mars", "Apr" => "Avril", "May" => "Mai", "Jun" => "Juin", "Jul" => "Juillet", "Aug" => "Aout", "Sept" => "Septembre", "Oct" => "Octobre", "Nov" => "Novembre", "Dec" => "Decembre");
						     $jours = array( 1 => "Lundi", 2 => "Mardi", 3 => "Mercredi", 4 => "Jeudi", 5 => "Vendredi", 6 => "Samedi", 7 => "Dimanche",);								
                                    
                                    $date = new DateTime($act->date);
                               
       								  $day= $date->format('j');
                                      $month = $date->format('M');
									  $year = $date->format('Y');
									  $this_month = $mons[$month];
									  $name = $date->format('N');
									  $this_day = $jours[$name];
									 
						  echo'   <tr>
                        <td class="agenda-date" class="active" rowspan="1"> <div class="dayofmonth">'.$day.'</div>  <div class="dayofweek">'.$this_day.'</div> <div class="shortdate text-muted">'.$this_month.', '.$year.'</div> </td>
                        <td>'.$act->act.'</td>
                        <td class="agenda-events"> <div class="agenda-event">'.$act->commentaire.' </div> </td>';
						$cltele = Clientele::find_clientele_by_matricule($act->matricule);
						echo '<td>'.$act->matricule.' '.$cltele->nom.' '.$cltele->prenom.'</td>
						<td>'.$inter->identite.' '.$inter->nom.' '.$inter->prenom.' Tel: '.$inter->fixe.' </td>';
						$clts = Client::find_clients_by_matricule($act->matfis);
						echo'<td> '.$act->matfis.' '.$clts->rs.' '.$clts->rc.' </td>
                    </tr>';
					
						   }
                      }						 
					  
					?>
                  
                </tbody>
            </table>
        </div>
    </div>
	</div>

										 </div>
										 
										   
									
									    <div id="contratshead" class="panel-heading" style="display:none">			              
                                           <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i>Contrats</h3>
								           </div> 
										<div id="contrats" style="display:none">
                                        <div class="panel-body">
										     <fieldset class="form-group">
										     <div class="col-xs-6">
											     <label id="att"  name="clt" for="exampleSelect1">Type de contrat</label>
                                                 <select class="form-control" name="client" id="mySelect4" onChange="valcontrat(this.value);">
												            <option value="">Choisir type de contrat</option>
															<option value="mobile">Mobile</option>
															<option value="fixe">Fixe</option>
															<option value="data">Data</option>
                                                 </select>
											 </div>	
										     </fieldset>
											 
										<div id="mobile" style="display:none">
										 <div class="panel-heading">			              
                                           <h3 class="panel-title1"><i class="fa fa-fw fa-edit"></i>Contrat Mobile</h3>
								           </div> 
										   <form name="ajmobile" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
										   
										   <fieldset class="form-group">
										     <div class="col-xs-6">
											     <label id="att"  name="clt" for="exampleSelect1">Clients</label>
                                                 <select class="form-control" name="clientmob" id="mySelect1">
												     <?php
													 $requete = "select * from clients where DRT='".$user->DRT."'";
													 $client = Client::find_all_query($requete);
													 $clt = new Client();
													 foreach($client as $clt){
						                        echo' <option value="'.$clt->matfis.'">'.$clt->rs.' '.$clt->rc.' '.$clt->activite.'</option>';
													 }
													?>
															
                                                 </select>
											 </div>	
										     </fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										   <label for="exampleSelect1">Code Client BSCS</label>
                                           <input class="form-control" type="text" name="bscsmob" value="" placeholder="Saisir Code Client BSCS">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Signature du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datedebutmob" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Dur&eacute;e d'engagement</label>
                                             <input class="form-control" type="text" name="dureemob" value="">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
                                              <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Fin du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datefinmob" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
									
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <label for="exampleSelect1" name ="nbrmob">Nombre de forfaits par offre</label>
											  <select class="form-control" name="nbremobile" id="nbre" onChange="valnbrmob(this.value);">
											  <option value="">Nombre de forfaits par offre</option>
										<?php	  
											  $max = 11;
											  for ($i=1 ; $i<$max ; $i++){
											  echo'<option value="'.$i.'">'.$i.' </option>';
											  }
										?>
											  </select>
											  </div>	
									 	</fieldset>
										
									   <div id="divdiv" style="display:block">
									   
									   <fieldset class="form-group" id="mob0" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre0" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre0"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob1" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre1" id="sel1">
                                                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre1"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob2" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre2" id="sel1">
								                 <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre2"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob3" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre3" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre3"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob4" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre4" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre4"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob5" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre5" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre5"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob6" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre6" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre6"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob7" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre7" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre7"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob8" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre8" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre8"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="mob9" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offre9" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresmob as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombre9"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										</div>

										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">CA mensuel</label>
                                            <input class="form-control" type="text" name="camob" value="" placeholder="Saisir CA mensuel">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">Remise</label>
                                            <input class="form-control" type="text" name="remisemob" value="" placeholder="Entrer Remise">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Geste</label>
                                             <input class="form-control" type="text" name="gestemob" value="" placeholder="Saisir Geste">
										</div>	
										</fieldset>
										<button type="submit" name="ajoutermobile" class="btn btn-primary">Valider Contrat</button>
										</form>
										</div>
										
										<div id="fixe" style="display:none">
										 <div class="panel-heading">			              
                                           <h3 class="panel-title1"><i class="fa fa-fw fa-edit"></i>Contrat Fixe</h3>
								           </div> 
										   <form name="ajfixe" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
										   
										   <fieldset class="form-group">
										     <div class="col-xs-6">
											     <label id="att"  name="clt" for="exampleSelect1">Clients</label>
                                                 <select class="form-control" name="clientfix" id="mySelect1">
												     <?php
													 $requete = "select * from clients where DRT='".$user->DRT."'";
													 $client = Client::find_all_query($requete);
													 $clt = new Client();
													 foreach($client as $clt){
						                        echo' <option value="'.$clt->matfis.'">'.$clt->rs.' '.$clt->rc.' '.$clt->activite.'</option>';
													 }
													?>
															
                                                 </select>
											 </div>	
										     </fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										   <label for="exampleSelect1">Code Client BSCS</label>
                                           <input class="form-control" type="text" name="bscsfix" value="" placeholder="Saisir Code Client BSCS">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Signature du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datedebutfix" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Dur&eacute;e d'engagement</label>
                                             <input class="form-control" type="text" name="dureefix" value="">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
                                              <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Fin du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datefinfix" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <label for="exampleSelect1" name ="nbrfix">Nombre de forfaits par offre</label>
											  <select class="form-control" name="nbrefix" id="nbrefix" onChange="valnbrfixe(this.value);">
											  <option value="">Nombre de forfaits par offre</option>
										<?php	  
											  $max = 11;
											  for ($i=1 ; $i<$max ; $i++){
											  echo'<option value="'.$i.'">'.$i.' </option>';
											  }
									    ?>
											   </select>
									    </div>	
									 	</fieldset> 
										
										<div id="divdivfix" style="display:block">
									   
									   <fieldset class="form-group" id="fix0" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix0" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix0"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix1" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix1" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix1"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix2" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix2" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix2"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix3" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix3" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix3"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix4" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix4" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix4"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix5" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix5" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix5"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix6" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix6" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix6"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix7" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix7" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix7"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix8" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix8" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix8"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="fix9" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offrefix9" id="sel1">
								                  <?php
								                       $off = new Forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresfix as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombrefix9"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										</div>
										
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">CA mensuel</label>
                                            <input class="form-control" type="text" name="cafix" value="" placeholder="Saisir CA mensuel">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">Remise</label>
                                            <input class="form-control" type="text" name="remisefix" value="" placeholder="Entrer Remise">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Geste</label>
                                             <input class="form-control" type="text" name="gestefix" value="" placeholder="Saisir Geste">
										</div>	
										</fieldset>
										<button type="submit" name="ajouterfixe" class="btn btn-primary">Valider Contrat</button>
										</form>
										</div>
										
										<div id="data" style="display:none">
										 <div class="panel-heading">			              
                                           <h3 class="panel-title1"><i class="fa fa-fw fa-edit"></i>Contrat Data</h3>
								           </div> 
										   <form name="ajaction" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
										   
										   <fieldset class="form-group">
										     <div class="col-xs-6">
											     <label id="att"  name="clt" for="exampleSelect1">Clients</label>
                                                 <select class="form-control" name="clientdata" id="mySelect1">
												     <?php
													 $requete = "select * from clients where DRT='".$user->DRT."'";
													 $client = Client::find_all_query($requete);
													 $clt = new Client();
													 foreach($client as $clt){
						                        echo' <option value="'.$clt->matfis.'">'.$clt->rs.' '.$clt->rc.' '.$clt->activite.'</option>';
													 }
													?>
															
                                                 </select>
											 </div>	
										     </fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										   <label for="exampleSelect1">Compte de Facturation FTD</label>
                                           <input class="form-control" type="text" name="ftddata" value="" placeholder="Saisir Compte de Facturation FTD">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Signature du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datedebutdata" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Dur&eacute;e d'engagement</label>
                                             <input class="form-control" type="text" name="dureedata" value="">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
                                              <div class="control-group">
                                                  <label for="date-picker-2" class="control-label">Date Fin du Contrat</label>
                                                     <div class="controls">
                                                       <div class="input-group">
                                                       <input id="date-picker-2" type="date" name ="datefindata" class="date-picker form-control" />
                                                       <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                                       </label>
                                                       </div>
                                                     </div>
                                              </div>
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										      <label for="exampleSelect1" name ="nbr">Nombre de forfaits par offre</label>
											  <select class="form-control" name="nbredata" id="nbre" onChange="valnbrdata(this.value);">
											  <option value="">Nombre de forfaits par offre</option>
										<?php	  
											  $max = 11;
											  for ($i=1 ; $i<$max ; $i++){
											  echo'<option value="'.$i.'">'.$i.' </option>';
											  }
										?>
											   </select>
											   </div>	
									 	</fieldset>
											  
										<div id="divdivdata" style="display:block">
									   
									   <fieldset class="form-group" id="data0" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata0" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata0"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data1" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata1" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata1"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data2" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata2" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata2"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data3" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata3" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata3"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data4" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata4" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata4"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data5" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata5" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata5"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data6" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata6" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata6"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data7" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata7" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata7"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data8" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata8" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata8"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										<fieldset class="form-group" id="data9" style="display:none">
												  <div class="col-xs-4">
												  <label for="exampleSelect1" name ="nbr">Saisir Nbre de lignes</label>
                                                  <select class="form-control" name="offredata9" id="sel1">
								                  <?php
								                       $off = new forfait();
								                       echo'<option value="">Veuillez choisir un forfait </option>';
								                       foreach($offresdata as $off){									 
                                                       echo'<option value="'.$off->forfait.'">'.$off->forfait.'</option>' ;
								                       }
												  ?>
                                                  </select>
												  <input class="form-control" type="text" name="nombredata9"  placeholder="Nombre de lignes par offre">
										          </div>
										</fieldset>
										
										</div>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">CA mensuel</label>
                                            <input class="form-control" type="text" name="cadata" value="" placeholder="Saisir CA mensuel">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										    <label for="exampleSelect1">Remise</label>
                                            <input class="form-control" type="text" name="remisedata" value="" placeholder="Entrer Remise">
										</div>	
										</fieldset>
										
										<fieldset class="form-group">
										<div class="col-xs-6">
										     <label for="exampleSelect1">Geste</label>
                                             <input class="form-control" type="text" name="gestedata" value="" placeholder="Saisir Geste">
										</div>	
										</fieldset>
										<button type="submit" name="ajouterdata" class="btn btn-primary">Valider Contrat</button>
										</form>
										</div>
										
										
										</div>
										</div>

	
            </div>
            </div>
			</div>
                                

           
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src='../style/js/footable.js'></script>
	<script type="text/javascript" src='../style/js/footable.min.js'></script>
	
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.calendar.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>

	
		<script type="text/javascript">	
	function val(value) {
		
		if (value == "a") {
        document.getElementById("spanco").style.display = 'block';
		document.getElementById("spancohead").style.display = 'block';
        document.getElementById("contrats").style.display = 'none';
		document.getElementById("contratshead").style.display = 'none';
		document.getElementById("calendar").style.display = 'none';
		document.getElementById("calendarhead").style.display = 'none';
		 
    } 
    else if (value == "b") {
        document.getElementById("contrats").style.display = 'block';
		document.getElementById("contratshead").style.display = 'block';
        document.getElementById("spanco").style.display = 'none';
		document.getElementById("spancohead").style.display = 'none';
		document.getElementById("calendar").style.display = 'none';
		document.getElementById("calendarhead").style.display = 'none';
    }
	else if (value == "c") {
        document.getElementById("contrats").style.display = 'none';
		document.getElementById("contratshead").style.display = 'none';
        document.getElementById("spanco").style.display = 'none';
		document.getElementById("spancohead").style.display = 'none';
		document.getElementById("calendar").style.display = 'block';
		document.getElementById("calendarhead").style.display = 'block';
    }
        

    }
	</script>
	
	<script type="text/javascript">	
	function valcontrat(value) {
		
		if (value == "mobile") {
        document.getElementById("mobile").style.display = 'block';
		document.getElementById("fixe").style.display = 'none';
        document.getElementById("data").style.display = 'none';
		
    } 
    else if (value == "fixe") {
        document.getElementById("fixe").style.display = 'block';
		document.getElementById("mobile").style.display = 'none';
        document.getElementById("data").style.display = 'none';
    }
	else if (value == "data") {
        document.getElementById("data").style.display = 'block';
		document.getElementById("mobile").style.display = 'none';
        document.getElementById("fixe").style.display = 'none';
    }
        

    }
	</script>
	
	<script type="text/javascript">	
	function valnbrmob(value) {
		
		for(var i =0 ; i < value ; i++) {
			 document.getElementById("mob"+i).style.display = 'block';
		}      

    }
	</script>
	
		<script type="text/javascript">	
	function valnbrfixe(value) {
		
		for(var i =0 ; i < value ; i++) {
			 document.getElementById("fix"+i).style.display = 'block';
		}      

    }
	</script>
	
		<script type="text/javascript">	
	function valnbrdata(value) {
		
		for(var i =0 ; i < value ; i++) {
			 document.getElementById("data"+i).style.display = 'block';
		}      

    }
	</script>
	
	<script>
$(document).ready(function(){
    $('#action').dataTable();


	
});
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#exportButton").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#action",
                schema: {
                    type: "table",
                    fields: {
                        Date: {type : String},
						Action: {type : String},
						Commentaire: {type : String},
						CC: {type: String},
						Interlocuteur: {type: String},
						Client: {type: String}
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "Date", title: "Date", width: 140 },
						{ field: "Action", title: "Action", width: 100 },
						{ field: "Commentaire", title: "Commentaire", width: 100 },
						{ field: "CC", title: "Charge Clientele", width: 120 },
						{ field: "Interlocuteur", title: "Interlocuteur", width: 120 },
						{ field: "Client", title: "Client Affaires", width: 120 }
                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "CalendrierActions"
                });
            });
        });
    });
</script>

	<script>
$("#exportButton").hover(
function () {
  
   $(this).text("PDF");
});
</script>
	

	
	

</body>

</html>

