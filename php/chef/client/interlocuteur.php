<?php require_once("init.php"); ?>

<?php

Class Interlocuteur {
	
	public $id_int;
	public $nom;
	public $prenom;
	public $piece;
	public $identite;
	public $poste;
	public $fixe;
	public $mobile;
	public $email;
	public $matfis;
	

	

public static function find_interlocuteur_by_identite($iden){
global $database;
$requete = "SELECT * FROM pfe.interlocuteur where indentite="."'".$iden."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_interlocuteur(){

global $database;
$requete = "INSERT INTO pfe.interlocuteur (nom, prenom ,piece,identite,poste,fixe,mobile,email,matfis) VALUES ('".$database->escape_string($this->nom)."', '".$database->escape_string($this->prenom)."' , '".$database->escape_string($this->piece)."', '".$database->escape_string($this->identite)."','".$database->escape_string($this->poste)."','".$database->escape_string($this->fixe)."','".$database->escape_string($this->mobile)."','".$database->escape_string($this->email)."','".$database->escape_string($this->matfis)."')" ;

if ($database->query($requete)){
	    $this->id_int = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_interlocuteur() {

global $database;
$requete = "UPDATE pfe.interlocuteur SET nom = '".$this->nom."', prenom = '".$this->prenom."', identite ='".$this->identite."' , piece = '".$database->escape_string($this->piece)."' , poste = '".$database->escape_string($this->poste)."', fixe = '".$database->escape_string($this->fixe)."', mobile = '".$database->escape_string($this->mobile)."', email = '".$database->escape_string($this->email)."' , matfis ='".$database->escape_string($this->matfis)."' WHERE id_int ='".$this->id_int."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_interlocuteur(){
	
global $database;
$requete = "DELETE FROM pfe.interlocuteur WHERE interlocuteur.id_int ='".$this->id_int."'"	;
$sql= "ALTER TABLE pfe.interlocuteur AUTO_INCREMENT = ".$this->id_int.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

