<?php require_once("init.php"); ?>

<?php

Class Contrats {
	
	public $id_contrat;
	public $code;
	public $matfis;
	public $id_cc;
	public $type;
	public $etat;
    public $signature;
	public $duree;
	public $fin;
	public $nbre_offres;
	public $ca_mensuel;
	public $remise;
	public $geste;
	public $DRT;

	

public static function find_contrat_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.contrats where id_contrat="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_contrat(){

global $database;
$requete = "INSERT INTO pfe.contrats (code, matfis , id_cc , type , etat , signature , duree , fin , nbre_offres ,ca_mensuel , remise , geste, DRT ) VALUES ('".$database->escape_string($this->code)."', '".$database->escape_string($this->matfis)."' ,'".$database->escape_string($this->id_cc)."', '".$database->escape_string($this->type)."' , '".$database->escape_string($this->etat)."' ,'".$database->escape_string($this->signature)."', '".$database->escape_string($this->duree)."','".$database->escape_string($this->fin)."', '".$database->escape_string($this->nbre_offres)."' ,'".$database->escape_string($this->ca_mensuel)."','".$database->escape_string($this->remise)."','".$database->escape_string($this->geste)."','".$database->escape_string($this->DRT)."')" ;

if ($database->query($requete)){
	    $this->id_contrat = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_contrat() {

global $database;
$requete = "UPDATE pfe.contrats SET type= '".$database->escape_string($this->type)."', etat='".$database->escape_string($this->etat)."' , signature = '".$this->signature."', duree = '".$this->duree."', fin = '".$database->escape_string($this->fin)."', nbre_offres='".$database->escape_string($this->nbre_offres)."' , ca_mensuel = '".$database->escape_string($this->ca_mensuel)."', remise = '".$database->escape_string($this->remise)."', geste = '".$database->escape_string($this->geste)."' , DRT ='".$database->escape_string($this->DRT)."' WHERE id_contrat ='".$this->id_contrat."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_contrat(){
	
global $database;
$requete = "DELETE FROM pfe.contrats WHERE contrats.id_contrat ='".$this->id_contrat."'"	;
$sql= "ALTER TABLE pfe.contrats AUTO_INCREMENT = ".$this->id_contrat.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

