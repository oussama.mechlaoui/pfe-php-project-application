<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../login.php'); } else {
	        global $database;
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			$sql = "select * from categories";
			$requete = "select * from offres";
			$requetespace = "select * from espacett where DRT='".$user->DRT."'";
			$categories = Categorie::find_all_query($sql);
			$offres = Offre::find_all_query($requete);
			$espace = Espace::find_all_query($requetespace);
		    
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT espace - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivision Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo' Chef Espace TT - '. $user->espace ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
              
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="../client/ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="../client/infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="../client/forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="../client/spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="../client/infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="../consultation/conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="../consultation/consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Ventes</li>
                  <li class="active">Liste des Ventes</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
             <br> <br>
				<div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-edit"></i> Ventes</h3>
                            </div>
                            <div class="panel-body">
              <form name="consventes" action="<?php $_SERVER['PHP_SELF'] ?>" method=POST>
		
                        <fieldset class="form-group">
                                 <label for="exampleSelect1">Categories</label>
                                 <select class="form-control" name="cat" id="exampleSelect1">
								 <option value="">Toutes les categories </option>
								 <?php
                                 $cat = new Categorie();
								 foreach($categories as $cat){
									 
                                       echo'<option value="'.$cat->categorie.'">'.$cat->categorie.'</option>' ;
								 }
									   ?>
                                 </select>
                        </fieldset>
						
						<fieldset class="form-group">
                                 <label for="exampleSelect1">Offres</label>
                                 <select class="form-control" name="off" id="exampleSelect1">
								 <option value="">Toutes les offres </option>
								 <?php
                                 $off = new Offre();
								 foreach($offres as $off){
									 
                                       echo'<option value="'.$off->nom.'">'.$off->nom.'</option>' ;
								 }
									   ?>
                                 </select>
                        </fieldset>
						
						<fieldset class="form-group">
                                 <label for="exampleSelect1" id="sel" >Espace TT</label>
                                 <select class="form-control" name="espacett" id="sel1">
								 <?php
								 $esp = new Espace();
								   echo'<option value="all">DRT </option>';
								 foreach($espace as $esp){
									 
                                       echo'<option value="'.$esp->espace.'">'.$esp->espace.'</option>' ;
								 }
								 ?>
								 </select>
                        </fieldset>

                       <div class="date-form">
    
                       <div class="form-horizontal">
    
                             <div class="control-group">
                                 <label for="date-picker-2" class="control-label">Date 1</label>
                                 <div class="controls">
                                 <div class="input-group">
                                 <input id="date-picker-2" type="date" name ="date1" class="date-picker form-control" />
                                 <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                                 </label>
                                 </div>
                                 </div>
                             </div>
                          
						  <div class="control-group">
                               <label for="date-picker-3" class="control-label">Date 2</label>
                               <div class="controls">
                               <div class="input-group">
                               <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                               </label>
                               <input id="date-picker-3" type="date" name = "date2" class="date-picker form-control" />
                               </div>
                               </div>
                               </div>
                           </div>    
                         </div> </br>
 
                        <div class="input-group-btn">
                <button class="btn btn-default"  name ="rechercher" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
              </form> <br>
			  <br>
			  <?php 
			  if (isset($_POST['rechercher'])){
			  if($_POST['espacett'] == "all"){
				  if (empty($_POST['date1']) && empty($_POST['date2']) && empty($_POST['cat']) && empty($_POST['off'])) {
					$req = "select * from pfe.ventes where DRT='".$user->DRT."'";
				} 
			else if(empty($_POST['date1']) && empty($_POST['date2']) && !empty($_POST['cat']) && !empty($_POST['off'])) {	
				  $req = "select * from pfe.ventes where offre='".$_POST['off']."' or categorie='".$_POST['cat']."' and DRT='".$user->DRT."' ";
			  }
			  else if (empty($_POST['date1']) && empty($_POST['date2']) && !empty($_POST['cat']) && empty($_POST['off'])){
				  $req = "select * from pfe.ventes where categorie='".$_POST['cat']."' and DRT='".$user->DRT."' ";
			  }
			  else if (empty($_POST['date1']) && empty($_POST['date2']) && empty($_POST['cat']) && !empty($_POST['off'])){
				  $req = "select * from pfe.ventes where offre='".$_POST['off']."' and DRT='".$user->DRT."' ";
			  }
			  else if ( !empty($_POST['date1']) && !empty($_POST['date2']) && empty($_POST['cat']) && empty($_POST['off'])) {
				  $req = "select * from pfe.ventes where date between '".$_POST['date1']."' and '".$_POST['date2']."' and DRT='".$user->DRT."' ";
			  }
			  else {
				  $req = "select * from pfe.ventes where date between '".$_POST['date1']."' and '".$_POST['date2']."' and offre='".$_POST['off']."' or categorie='".$_POST['cat']."' and DRT='".$user->DRT."' ";
			  }
			  }
			  else {	  
				if (empty($_POST['date1']) && empty($_POST['date2']) && empty($_POST['cat']) && empty($_POST['off'])) {
					$req = "select * from pfe.ventes where espacett='".$_POST['espacett']."' and DRT='".$user->DRT."' ";
				} 
			else if(empty($_POST['date1']) && empty($_POST['date2']) && !empty($_POST['cat']) && !empty($_POST['off'])) {	
				  $req = "select * from pfe.ventes where offre='".$_POST['off']."' or categorie='".$_POST['cat']."' and espacett='".$_POST['espacett']."' and DRT='".$user->DRT."' ";
			  }
			  else if (empty($_POST['date1']) && empty($_POST['date2']) && !empty($_POST['cat']) && empty($_POST['off'])){
				  $req = "select * from pfe.ventes where categorie='".$_POST['cat']."' and espacett='".$_POST['espacett']."' and DRT='".$user->DRT."' ";
			  }
			  else if (empty($_POST['date1']) && empty($_POST['date2']) && empty($_POST['cat']) && !empty($_POST['off'])){
				  $req = "select * from pfe.ventes where offre='".$_POST['off']."' and espacett='".$_POST['espacett']."' and DRT='".$user->DRT."' ";
			  }
			  else if ( !empty($_POST['date1']) && !empty($_POST['date2']) && empty($_POST['cat']) && empty($_POST['off'])) {
				  $req = "select * from pfe.ventes where date between '".$_POST['date1']."' and '".$_POST['date2']."' and espacett='".$_POST['espacett']."' and DRT='".$user->DRT."' ";
			  }
			  else {
				  $req = "select * from pfe.ventes where date between '".$_POST['date1']."' and '".$_POST['date2']."' and offre='".$_POST['off']."' and espacett='".$_POST['espacett']."' or categorie='".$_POST['cat']."' and DRT='".$user->DRT."' ";
			  }
			  }
				  $ventes = Vente::find_all_query($req);
				  
				  if($ventes == null){
					  echo '<script language="Javascript">
                <!--
                alert("Vente non trouv�e");
				document.location.replace("listventechef.php");
                // -->
				  </script>';}
				  else {
				  ?>
				   <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> Consulter Ventes</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
								<button type="button" id="bout" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button> <br> </br>
                                               <table id="ventes" class="table table-bordered table-condensed table-hover table-striped display responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>Offres</th>
                                                <th>Quantite</th>
                                                <th>Categorie</th>
												<th>EspaceTT</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									<?php	
									$sales = new Vente();
										foreach($ventes as $sales){
                                           echo'<tr>';
                                           echo'<td>'.$sales->offre.'</td>';
                                           echo'<td>'.$sales->quantite.'</td>';
                                           echo'<td>'.$sales->categorie.'</td>';
										   echo'<td>'.$sales->espacett.'</td>';
                                           echo'<td>'.$sales->date.'</td>';
                                            echo'</tr>';
										}
										echo'
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
				  
			  } 
			  }  
			  
			  ?>
                            </div>
                        </div>
                    </div>
                </div>

             
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../style/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="bootstrap-datepicker.de.js" charset="UTF-8"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>



<script type="text/javascript">	
    jQuery(function ($) {
        $("#bout").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#ventes",
                schema: {
                    type: "table",
                    fields: {
                        Offres: { type : String },
					    Quantite: { type : Number },
						Categorie: { type : String },
						EspaceTT: { type : String },
						Date: { type : String }
                        
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "landscape");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "Offres", title: "Offres", width: 120 },
						{ field: "Quantite", title: "Quantite", width: 120 },
						{ field: "Categorie", title: "Categorie", width: 120 },
						{ field: "EspaceTT", title: "EspaceTT", width: 120 },
						{ field: "Date", title: "Date", width: 120 }
                        

                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "Liste_des_ventes"
                });
            });
        });
    });
</script>

<script>
$("#bout").hover(
function () {
  
   $(this).text("PDF");
});
</script>	
	
</body>

</html>
