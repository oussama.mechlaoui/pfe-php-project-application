<?php require_once("init.php"); ?>

<?php

Class Vente {
	
	public $id_vente;
	public $offre;
	public $quantite;
	public $categorie;
	public $espacett;
	public $DRT;
	public $date;

public static function find_vente_by_id($id){
global $database;
$requete = "SELECT * FROM pfe.ventes where id_vente="."'".$id."'";
$row = self::find_all_query($requete);
return !empty($row) ? array_shift($row) : false;	
}
	
public static function find_all_query($sql){
	global $database;
	$RS = $database->query($sql);
	$array = array();
	
while ($row = mysqli_fetch_array($RS)) {
$array[] = self::instantiation($row); }
return $array;

}

public static function instantiation($row) {
	
	$object = new self;
	foreach ($row as $attribut => $val) {
		
	    if ($object->has_the_attribute($attribut)) {
			$object->$attribut = $val ;
		}
	}
	
	return $object;	
}

public function has_the_attribute($attribut) {
	$properties = get_object_vars($this) ;
	return array_key_exists($attribut, $properties) ;
}

public function add_vente(){

global $database;
$requete = "INSERT INTO pfe.ventes (offre, quantite,categorie,espacett,DRT,date) VALUES ('".$database->escape_string($this->offre)."','".$database->escape_string($this->quantite)."','".$database->escape_string($this->categorie)."', '".$database->escape_string($this->espacett)."','".$database->escape_string($this->DRT)."' ,'".$database->escape_string($this->date)."')" ;

if ($database->query($requete)){
	    $this->id_vente = $database->the_insert_id();
		return true;
	}else {
		return false;
	}

}

public function update_vente() {

global $database;
$requete = "UPDATE pfe.ventes SET offre = '".$this->categorie."', quantite = '".$database->escape_string($this->quantite)."', espacett = '".$database->escape_string($this->espacett)."', DRT='".$database->escape_string($this->DRT)."' ,date = '".$database->escape_string($this->date)."' WHERE id_vente ='".$this->id_vente."'"	;

if ($database->query($requete)) {
	return true;
}else { return false;      }
	
}

public function delete_vente(){
	
global $database;
$requete = "DELETE FROM pfe.ventes WHERE ventes.id_vente ='".$this->id_vente."'"	;
$sql= "ALTER TABLE pfe.ventes AUTO_INCREMENT = ".$this->id_vente.""	;

$database->query($requete);
$database->query($sql);
return (mysqli_affected_rows($database->cnx) == 1) ? true : false;
	
}

}


?>

