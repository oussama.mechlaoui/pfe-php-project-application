<?php
require_once("init.php");
?>
<?php

if (!$session->signed_in()) { header('location: ../../login.php'); } else {
			$user = Utilisateur::find_user_by_id($session->user_id) ;
                        if ($user->profil != "chef")
                        {
                                header('location: ../../'.$user->profil.'/'.$user->profil.'.php');
                        }
			$requete = "SELECT * FROM pfe.categories";
			$categorie = Categorie::find_all_query($requete);
		 
			
}

//$user = User::verify($session->login, $session->password); if ($user) {echo $user->login; echo'yes';}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TT Admin - Tableau de Bord Commercial</title>

    <!-- Bootstrap Core CSS -->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet">
    <link href="../style/css/halflings.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../style/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../style/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
     <link href="../style/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../style/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="../style/css/all.min.css" />
	
	
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
			<a class="navbar-brand1" ><img src="../../admin/parametrer/images/logo.png"  height="50" width="200"> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo '../'.$user->profil.'.php'?>"><?php if ($user->profil == 'admin') 
				                                                                            { echo'Administrateur';}
																					   else if ($user->profil == 'chef')
																					        { echo 'Subdivison Commerciale: '.$user->DRT.'' ;}
																				       else if ($user->profil == 'agence')
																					        { echo'Espace TT - '. $user->espace ;} ?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->nom.' '.$user->prenom; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
           <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   <li class="active">
                        <a href="<?php echo '../'.$user->profil.'.php' ?>"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-group"></i> Gestion Clients Affaires <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="collapse">
                           <li>
                                <a href="../client/ajclient.php">Ajout Client Affaires</a>
                            </li>
                            <li>
                                <a href="../client/infclient.php">Consultation Client Affaires</a>
                            </li>
							<li>
                                <a href="../client/forfait.php">Forfaits</a>
                            </li>
							<li>
                                <a href="../client/spanco.php">SPANCO</a>
                            </li>
							<li>
                                <a href="../client/infcontrats.php">D&eacute;tails Contrats</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-ol"></i> Cat&eacute;gories/Offres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="collapse">
                           <li>
                                <a href="conscatchef.php">Consultation Cat&eacute;gories</a>
                            </li>
                            <li>
                                <a href="consoffchef.php">Consultation Offres</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="glyphicon glyphicon-pushpin"></i> Objectifs <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="collapse">
						<li>
                                <a href="../objectifs/objectifchef.php">Fixer Objectifs</a>
                            </li>
                        <li>
                                <a href="../objectifs/suivichef.php">Suivi Objectifs</a>
                            </li>
						</ul>
					</li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-table"></i> Ventes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="collapse">
                            <li>
                                <a href="../vente/listventechef.php">Suivi des Ventes</a>
                            </li>
                        </ul>
                    </li>
					<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-bar-chart-o"></i> Statistiques <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="collapse">
                            <li>
                                <a href="../statistiques/statschef.php">Stats</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<ol class="breadcrumb">
                  <li><a href="<?php echo '../'.$user->profil.'.php'?>">Acceuil</a></li>
                  <li class="active" >Cat&eacute;gories/Offres</li>
                  <li class="active">Consultation Categories</li>
                </ol>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <br> <br>

                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-fw fa-desktop"></i> Consulter Categories</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
								<button type="button" id="bout" class="btn btn-primary btn-circle"><i class="glyphicon glyphicon-list"></i></button> <br> </br>
                                    <table id="myTable" class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Categories</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php 
										$cat = new Categorie();
										foreach($categorie as $cat){
                                           echo'<tr>';
                                           echo'<td>'.$cat->categorie.'</td>';
                                           echo'<td>'.$cat->description.'</td>';
										   echo'</tr>';
										   echo'</tr>';
										}
											?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../style/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../style/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../style/js/plugins/morris/raphael.min.js"></script>
    <script src="../style/js/plugins/morris/morris.min.js"></script>
    <script src="../style/js/plugins/morris/morris-data.js"></script>
	<script src='../style/js/jquery.dataTables.min.js'></script>
	
	<script src="../style/js/jquery.min.js"></script>
	<script type="text/javascript" src="../style/js/jquery.dataTables.min.cat.js"></script>
    <script type="text/javascript" src="../style/js/dataTables.responsive.min.js"></script>
	
	<script type="text/javascript" src="../style/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="../style/js/jszip.min.js"></script>


<script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>

<script type="text/javascript">	
    jQuery(function ($) {
        $("#bout").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#myTable",
                schema: {
                    type: "table",
                    fields: {
                        Categories: { type : String },
					    Description: { type : String }
						
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "Bootstrap",
                    created: new Date()
                });
                  
                pdf.addPage("a4", "portrait");

                pdf.table(
                    50,
                    50,
                    data,
                    [
					    { field: "Categories", title: "Categorie", width: 120 },
						{ field: "Description", title: "Description", width: 350 }           
                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "Liste_des_Categories"
                });
            });
        });
    });
</script>

<script>
$("#bout").hover(
function () {
  
   $(this).text("PDF");
});
</script>


</body>

</html>
