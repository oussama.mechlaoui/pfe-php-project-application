<?php

Class Session {
	
	private $signed_in = false;
	public  $user_id;
	public  $profil;
	public  $DRT;

	
function  __construct(){
        session_save_path("/var/www/php/session");
	session_start();
	$this->check();	
	
}	

private function check(){
	
	if (isset($_SESSION['user_id'])){
		$this->user_id = $_SESSION['user_id'];
		$this->signed_in = true;		
	} else {
		unset($this->user_id);
		$this->signed_in = false;
		
	}
}

public function signed_in(){
	
	return $this->signed_in;
	
}

public function login($user) {
	if ($user) {	
		$this->user_id = $_SESSION['user_id'] = $user->id ;
		$this->signed_in = true;
		$this->profil = $user->profil;
		$this->DRT = $user->DRT;
		header('location: '.$user->profil.'/'.$user->profil.'.php');
	}
}

public function logout() {
	
	unset($_SESSION['user_id']);
	unset($this->user_id);
	$this->signed_in = false;
}


}
$session = new Session();
?>
